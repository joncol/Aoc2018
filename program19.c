/* Take 1
 0   addi ip 16 5 //     jmp E
 1   seti  1 7 3  // A:  reg3 = 1
 2   seti  1 4 1  // A': reg1 = 1
 3   mulr  3 1 4  // B:  reg4 = reg3 * reg1 (1*1=1, 1*2=2)
 4   eqrr  4 2 4  //     reg4 = if reg4 == reg2 then 1 else 0 (0,0,...)
 5   addr  4 5 5  //     ip += reg4
 6   addi ip 1 5  //     ip += 1
 7   addr  3 0 0  //     reg0 += reg3
 8   addi  1 1 1  //     reg1 += 1 (2, 3, )
 9   gtrr  1 2 4  //     reg4 = if reg1 > reg2 then jmp D else jmp B
10   addr ip 4 5  //     __ip =+ reg4
11   seti  2 1 5  //     __jmp B
12   addi  3 1 3  // D:  reg3 += 1
13   gtrr  3 2 4  //     if reg3 > reg2 then exit else jmp A'
14   addr  4 5 5  //     __ip += reg4
15   seti  1 4 5  //     __jmp A'
16   mulr ip 5 5  //     __exit
17   addi  2 2 2  // E:  __reg2 += 2
18   mulr  2 2 2  //     __reg2 *= 2
19   mulr ip 2 2  //     __reg2 *= ip
20   muli  2 11 2 //     __reg2 *= 11  ... <=> reg2 = 836
21   addi  4 1 4  //     __reg4 += 1
22   mulr  4 5 4  //     reg4 = (reg4+1)*22 + 19  (41)
23   addi  4 19 4 //     __reg4 += 19
24   addr  2 4 2  //     reg2 += reg4 (reg2=877)
25   addr ip 0 5  //     ip += reg0
26   seti  0 9 5  //     ...jmp A
27   setr ip 7 4  //     __reg4 = 27
28   mulr  4 5 4  //     __reg4 *= 28
29   addr ip 4 4  //     __reg4 += 29
30   mulr ip 4 4  //     __reg4 *= 30
31   muli  4 14 4 //     __reg4 *= 14
32   mulr  4 5 4  //     __reg4 *= 32    <=> reg4 = 10550400
33   addr  2 4 2  //     __reg2 += reg4  <=> reg2 = 10551277
34   seti  0 9 0  //     reg0 = 0
35   seti  0 6 5  //     jmp A
*/

/* Take 2: Remove stale code, and simplify
   reg2 = 10551277 or reg2 = 877 (depending on initial value of reg0)

 1   seti  1 7 3  // A:  reg3 = 1
 2   seti  1 4 1  // A': reg1 = 1
 3   mulr  3 1 4  // B:  reg4 = reg3 * reg1 (1*1=1, 1*2=2)
 4   eqrr  4 2 4  //     reg4 = if reg4 == reg2 then 1 else 0 (0,0,...)
 5   addr  4 5 5  //     ip += reg4
 6   addi ip 1 5  //     ip += 1
 7   addr  3 0 0  //     reg0 += reg3
 8   addi  1 1 1  //     reg1 += 1 (2, 3, )
 9   gtrr  1 2 4  //     reg4 = if reg1 > reg2 then jmp D else jmp B
12   addi  3 1 3  // D:  reg3 += 1
13   gtrr  3 2 4  //     if reg3 > reg2 then exit else jmp A'
*/

/* Take 3: Only pseudocode. Assume reg0 = x, reg1 = i, reg2 = limit, reg3 = j
   limit = 10551277 or 877
   j = 1

   A:
     i = 1

   B:
     if i*j == limit
       then x += j

     i += 1
     if i > limit
       then j += 1
            if j > limit
              then exit
              else jmp A
       else jmp B
*/

/* Take 4: Refactor pseudocode
   limit = 10551277 or 877
   j = 1

   A:
     i = 1

   B:
     if i*j == limit
       then x += j

     i += 1

     if i > limit
       then if (j += 1) > limit
              then exit
              else jmp A
       else jmp B
*/

/* OK, it sums all prime factors of `limit`, including the trivial factors 1 and
   the number itself. So for 877 (which is prime) x = 1 + 877 = 878

   Prime factorization of 10551277 = 11 * 959207, so x = 11 + 959207 = 959218.
   So for 10551277, x = 1 + 10551277 + 11 + 959207 = 11510496.
*/
