module InventoryManagementSystem where

import           Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import qualified Data.Text as T

letterFrequencies :: T.Text -> Map Char Int
letterFrequencies = T.foldr (\x -> Map.insertWith (+) x 1) Map.empty

checksum :: [T.Text] -> Int
checksum ss = count 2 * count 3
  where
    freqs = map letterFrequencies ss
    count x = length . filter (not . null) $ map (Map.filter (==x)) freqs

similarBoxes :: Ord a => [[a]] -> [a]
similarBoxes ss =
  let ((x, y), _) = head . Map.toList . Map.filter (==1) $ distances ss
  in commonElems x y

distances :: Ord a => [[a]] -> Map ([a], [a]) Int
distances ss = Map.fromList [((s1, s2), hammingDistance s1 s2) |
                             s1 <- ss, s2 <- ss, s1 /= s2]

commonElems :: Eq a => [a] -> [a] -> [a]
commonElems x y = foldr aux [] (zip x y)
  where aux (a, b) acc = if a == b then a:acc else acc

hammingDistance :: Eq a => [a] -> [a] -> Int
hammingDistance x y = length . filter (uncurry (/=)) $ zip x y
