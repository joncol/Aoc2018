{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE RecordWildCards #-}

module BeverageBandits where

import           Control.Arrow ((&&&))
import           Control.Applicative ((<|>))
import           Control.Lens (over, view)
import           Control.Monad (forM_, guard, when)
import           Data.Bool (bool)
import           Data.Function (on)
import           Data.Functor (($>))
import           Data.List (find, sortOn)
import           Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import           Data.Maybe (catMaybes, fromJust, isNothing, isJust, maybe)
import           Linear.V2 (V2(..), _x, _y)
import           Safe (headMay)
import           Text.Megaparsec as M
import           Text.Megaparsec.Char as M
import           UI.NCurses as UI

import           Util

type HitPoints = Int

newtype Coord = Coord (V2 Int) deriving (Eq)

instance Ord Coord where
  compare (Coord u) (Coord v) = (compare `on` (view _y &&& view _x)) u v

instance Show Coord where
  show (Coord (V2 x y)) = show (x, y)

data ParsedCell = PCWall | PCEmpty | PCGoblin | PCElf deriving (Eq, Show)

data Cell = Wall | Empty deriving (Eq, Show)

data UnitType = Elf | Goblin deriving (Eq, Show)

data Unit = Unit { unitType      :: UnitType
                 , unitHitPoints :: HitPoints
                 } deriving (Eq, Show)

type Grid = [[Cell]]

type Units = Map Coord Unit

data RoundType = Full | Partial deriving (Eq, Show)

data Round = Round { roundUnits :: Units
                   , roundType  :: RoundType
                   } deriving (Eq, Show)

data Action = StepForward | StepBackward | Quit deriving (Eq, Show)

goblinAttackPower :: HitPoints
goblinAttackPower = 3

initialHitPoints :: HitPoints
initialHitPoints = 200

gridParser :: Parser [[ParsedCell]]
gridParser = M.many cellParser `M.sepBy` M.newline

cellParser :: Parser ParsedCell
cellParser =
  M.char '#' $> PCWall <|>
  M.char '.' $> PCEmpty <|>
  M.char 'G' $> PCGoblin <|>
  M.char 'E' $> PCElf

withCoords :: [[a]] -> [(Coord, a)]
withCoords =
  concatMap (\(y, row) -> map (\(x, cell) -> (Coord $ V2 x y, cell)) $
                          zip [0..] row) . zip [0..]

getUnits :: [[ParsedCell]] -> Units
getUnits = foldr collectUnits Map.empty . withCoords
  where
    collectUnits (pos, cell) units =
      case cell of
        PCGoblin -> Map.insert pos (Unit Goblin initialHitPoints) units
        PCElf    -> Map.insert pos (Unit Elf initialHitPoints) units
        _        -> units

convertGrid :: [[ParsedCell]] -> Grid
convertGrid = map (map convertCell)

convertCell :: ParsedCell -> Cell
convertCell c =
  case c of
    PCWall   -> Wall
    PCEmpty  -> Empty
    PCGoblin -> Empty
    PCElf    -> Empty

runFight :: Grid -> Units -> Int
runFight = (fst .) . goFight 3
-- runFight = goFight 3

tuneFight :: Grid -> Units -> Int
tuneFight grid units =
  fst . fromJust . find (\(_, units') -> elfCount units' == initialElfCount) $
    allFights
  where
    elfCount = length . Map.keys . Map.filter ((==Elf) . unitType)
    initialElfCount = elfCount units
    allFights = map (\a -> goFight a grid units) [4..]

goFight :: HitPoints -> Grid -> Units -> (Int, Units)
goFight elfAttackPower grid units = (score, unitsLeft)
  where
    rounds = tail . iterate (playRound elfAttackPower grid . roundUnits) $
             Round units Partial
    (full, final:_) = span ((==Full) . roundType) rounds
    unitsLeft = roundUnits final
    score = sum (map unitHitPoints . Map.elems $ unitsLeft) * length full

runFightUI :: HitPoints -> Grid -> Units -> IO ()
runFightUI elfAttackPower grid units =
  UI.runCurses $ do
    UI.setEcho False
    w <- UI.defaultWindow
    mainLoopUI elfAttackPower grid units w

mainLoopUI :: HitPoints -> Grid -> Units -> Window -> Curses ()
mainLoopUI elfAttackPower grid units w = loop [Round units Partial] 0
  where
    loop :: [Round] -> Int -> Curses ()
    loop [] _ = return ()
    loop rounds@(gameRound:rest) roundCounter =
      do
        colorWhite <- UI.newColorID UI.ColorWhite UI.ColorBlack 2
        let units' = roundUnits gameRound
        renderCave grid units'
        let fullRounds = length . filter ((==Full) . roundType) $ rounds
        let score = sum (map unitHitPoints $ Map.elems units') * fullRounds
        UI.updateWindow w $ do
          UI.setColor colorWhite
          UI.moveCursor 2 0
          UI.drawString $ "Rounds played: " ++ show roundCounter ++
                          " score: " ++ show score ++ replicate 10 ' '
        renderUnitHitPoints grid units'
        UI.updateWindow w $ UI.moveCursor 0 0
        UI.render
        action <- handleKeyboardEvents w
        case action of
          Nothing -> loop rounds roundCounter
          Just Quit -> return ()
          Just StepBackward -> if null rest -- At beginning of history?
                               then loop rounds 0
                               else loop rest (roundCounter - 1)
          Just StepForward -> let round' = playRound elfAttackPower grid units'
                              in loop (round':rounds) (roundCounter + 1)

renderCave :: Grid -> Units -> UI.Curses ()
renderCave grid units =
  do
    w <- UI.defaultWindow
    colorYellow <- UI.newColorID UI.ColorYellow UI.ColorBlack 1
    colorWhite <- UI.newColorID UI.ColorWhite UI.ColorBlack 2
    colorCyan <- UI.newColorID UI.ColorCyan UI.ColorBlack 3
    colorGreen <- UI.newColorID UI.ColorGreen UI.ColorBlack 4
    colorMagenta <- UI.newColorID UI.ColorMagenta UI.ColorBlack 5
    UI.updateWindow w $ do
      UI.setColor colorWhite
      UI.moveCursor 0 0
      UI.drawString "Elfs vs Goblins (press q to quit)"
      forM_ [0..length grid - 1] $ \y ->
        forM_ [0..length (head grid) - 1] $ \x -> do
            UI.moveCursor (fromIntegral (4 + y)) (fromIntegral x)
            let unit = Map.lookup (Coord $ V2 x y) units
            case unit of
              Just u -> do
                          UI.setColor colorMagenta
                          let uc = unitTypeToChar . unitType $ u
                          when (uc == 'G') $ UI.setColor colorGreen
                          when (uc == 'E') $ UI.setColor colorMagenta
                          UI.drawString [uc]
              Nothing -> do
                           let cell = cellToChar $ getCell grid (Coord (V2 x y))
                           when (cell == '#') $ UI.setColor colorYellow
                           when (cell == '.') $ UI.setColor colorCyan
                           UI.drawString [cell]

renderUnitHitPoints :: Grid -> Units -> UI.Curses ()
renderUnitHitPoints grid units =
  do
    w <- UI.defaultWindow
    UI.updateWindow w $
      forM_ [0..height-1] $ \row -> do
        let xs = map (\(Coord (V2 x _)) -> x) . Map.keys .
                 Map.filterWithKey (\(Coord (V2 _ y)) _ -> y == row) $ units
        UI.moveCursor (fromIntegral $ 4+row) (fromIntegral width)
        UI.drawString $ replicate 30 ' '
        UI.moveCursor (fromIntegral $ 4+row) (fromIntegral width)
        forM_ xs $ \ x ->
          UI.drawString $ maybe "" ((" "++) . show . unitHitPoints) $
            Map.lookup (Coord $ V2 x row) units
  where
    width = length $ head grid
    height = length grid

renderCoords :: [Coord] -> UI.Curses ()
renderCoords coords =
  do
    colorWhite <- UI.newColorID UI.ColorWhite UI.ColorBlack 2
    w <- UI.defaultWindow
    UI.updateWindow w $ do
      UI.setColor colorWhite
      forM_ coords $ \(Coord (V2 x y)) -> do
        UI.moveCursor (fromIntegral (4 + y)) (fromIntegral x)
        UI.drawString "*"

handleKeyboardEvents :: Window -> Curses (Maybe Action)
handleKeyboardEvents w = do
  ev <- getEvent w Nothing
  case ev of
    Nothing  -> return Nothing
    Just ev' ->
      case ev' of
        EventSpecialKey KeyLeftArrow  -> return $ Just StepBackward
        EventCharacter 'h'            -> return $ Just StepBackward
        EventSpecialKey KeyRightArrow -> return $ Just StepForward
        EventCharacter 'l'            -> return $ Just StepForward
        EventCharacter 'q'            -> return $ Just Quit
        _                             -> return Nothing

cellToChar :: Cell -> Char
cellToChar c =
  case c of
    Wall  -> '#'
    Empty -> '.'

getCell :: Grid -> Coord -> Cell
getCell grid (Coord (V2 x y)) = (grid !! y) !! x

unitTypeToChar :: UnitType -> Char
unitTypeToChar Goblin = 'G'
unitTypeToChar Elf    = 'E'

playRound :: HitPoints -> Grid -> Units -> Round
playRound elfAttackPower grid units =
  foldl aux (Round units Full) $ Map.keys units
  where
    aux :: Round -> Coord -> Round
    aux gameRound@Round{..} pos
      | roundType == Partial = gameRound
      | otherwise = if Map.member pos roundUnits -- Is unit still alive?
                    then maybe (gameRound{roundType = Partial})
                               (flip Round Full) $
                               makeUnitTurn attackPower grid roundUnits pos
                    else gameRound
      where attackPower = case unitType <$> Map.lookup pos units of
                            Just Elf -> elfAttackPower
                            _ -> goblinAttackPower

-- Returns Nothing if there are no target units left.
makeUnitTurn :: HitPoints -> Grid -> Units -> Coord -> Maybe Units
makeUnitTurn attackPower grid units pos =
  do
    unit <- Map.lookup pos units
    let targets = getTargetCoords units $ unitType unit
    guard $ not (null targets)
    let inRange = getWithinRange grid units pos $ unitType unit
    if pos `elem` inRange
    then Just $ maybeAttackTarget grid units pos attackPower
    else let path = findShortestPathToTarget grid units pos
         in if null path
            then Just units
            else let pos' = path !! 1
                     units' = moveUnit units pos pos'
                 in return $ maybeAttackTarget grid units' pos' attackPower

getTargetCoords :: Units -> UnitType -> [Coord]
getTargetCoords units fromType =
  Map.keys . Map.filter ((/= fromType) . unitType) $ units

getWithinRange :: Grid -> Units -> Coord -> UnitType -> [Coord]
getWithinRange grid units ignorePos fromUnitType =
  concatMap (\pos ->
               getNeighbors grid pos
                            (\p -> p == ignorePos ||
                                        isNothing (Map.lookup p units))) $
            getTargetCoords units fromUnitType

getNeighbors :: Grid -> Coord -> (Coord -> Bool) -> [Coord]
getNeighbors grid (Coord pos) p =
  filter (\x -> getCell grid x == Empty && p x) (map Coord neighbors')
  where
    neighbors = map ($ pos) [ over _y (subtract 1)
                            , over _x (subtract 1)
                            , over _x (+1)
                            , over _y (+1) ]
    w = length (head grid)
    h = length grid
    neighbors' = filter (\(V2 x y) -> x >= 0 && x < w &&
                                      y >= 0 && y < h) neighbors

-- Uses breadth-first search to find the shortest path. If there are many
-- shortest paths, the path to the first target in reading order is returned.
findShortestPathToTarget :: Grid -> Units -> Coord -> [Coord]
findShortestPathToTarget grid units startPos =
  let (foundCoords, via) = go [startPos] [startPos] [] maxBound Map.empty
      paths = map (catMaybes . takeWhile isJust .
                   iterate (>>= flip Map.lookup via) . return) foundCoords
      minLength = minimum . map length $ paths
      minPaths = filter ((==minLength) . length) paths
      sorted = sortOn head minPaths
  in maybe [] reverse $ headMay sorted
  where
    fromUnit = fromJust $ Map.lookup startPos units
    fromType = unitType fromUnit
    targets = getTargetCoords units fromType

    go :: [Coord] -> [Coord] -> [Coord] -> Int -> Map Coord Coord
       -> ([Coord], Map Coord Coord)
    go [] _ found _ via = (found, via)
    go (p:ps) visited found minDist via =
      if | manhattanDistance startPos p > minDist -> (found, via)
         | p `elem` targets -> (go ps visited (p:found) minDist' via')
         | otherwise -> (go (ps ++ ns) (visited ++ ns) found minDist via')
      where
        minDist' = min (manhattanDistance startPos p) minDist
        ns = filter (\n -> n `notElem` visited &&
                           (unitType <$> Map.lookup n units) /= Just fromType) $
             getNeighbors grid p (const True)
        via' = foldl (\acc n -> Map.insert n p acc) via ns

manhattanDistance :: Coord -> Coord -> Int
manhattanDistance (Coord (V2 x1 y1)) (Coord (V2 x2 y2)) =
  abs (y2 - y1) + abs (x2 - x1)

moveUnit :: Units -> Coord -> Coord -> Units
moveUnit units fromPos toPos = Map.insert toPos unit (Map.delete fromPos units)
  where unit = fromJust $ Map.lookup fromPos units

maybeAttackTarget :: Grid -> Units -> Coord -> HitPoints -> Units
maybeAttackTarget grid units pos attackPower =
  maybe units (flip (Map.update attackTarget) units) maybeTarget
  where
    maybeTarget = selectTarget grid units pos
    attackTarget x = let hitPoints = unitHitPoints x - attackPower
                     in bool Nothing
                             (Just x{unitHitPoints = hitPoints}) $
                             hitPoints > 0

selectTarget :: Grid -> Units -> Coord -> Maybe Coord
selectTarget grid units pos =
  do
    unit <- Map.lookup pos units
    let targetType = if unitType unit == Elf then Goblin else Elf
    let neighbors = getNeighbors grid pos
                      (\p ->
                         (unitType <$> Map.lookup p units) == Just targetType)
    let targetUnits = map (id &&& fromJust . flip Map.lookup units) neighbors
    let sorted = sortOn (unitHitPoints . snd &&& fst) targetUnits
    fst <$> headMay sorted
