{-# LANGUAGE OverloadedStrings #-}

module SumOfItsParts where

import           Control.Arrow (second)
import           Control.Monad (void)
import           Data.Char (ord)
import           Data.List (delete)
import           Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import           Data.Maybe (isNothing)
import           Data.Tuple (swap)
import qualified Text.Megaparsec as M
import qualified Text.Megaparsec.Char as M

import           Util

type Step = Char
type StepDependency = (Step, Step)

data Event = Event { eventTime :: Int     -- Event time in seconds
                   , eventSteps :: [Step] -- The steps finished at this time
                   } deriving (Eq, Ord, Show)

stepDependencyParser :: Parser StepDependency
stepDependencyParser = do
  void $ M.string "Step "
  a <- M.anySingle
  void $ M.string " must be finished before step "
  b <- M.anySingle
  return (a, b)

executeSteps :: [StepDependency] -> [Step] -> [Step]
executeSteps stepDeps alphabet = aux (stepDependencies stepDeps) []
  where
    aux deps executed =
      case getNextSteps deps executed alphabet of
        []  -> executed
        n:_ -> aux (executeStep deps n) (executed ++ [n])

stepDependencies :: [StepDependency] -> Map Step [Step]
stepDependencies = Map.fromListWith (flip (++)) . map (second (:[]) . swap)

executeStep :: Map Step [Step] -> Step -> Map Step [Step]
executeStep deps step = Map.foldrWithKey aux deps deps
  where
    aux :: Step -> [Step] -> Map Step [Step] -> Map Step [Step]
    aux k _ = Map.update (Just . delete step) k

{-|
  Finds the next steps with no dependencies, that have not already been
  executed.
-}
getNextSteps :: Map Step [Step] -> [Step] -> [Step] -> [Step]
getNextSteps deps alreadyExecuted alphabet =
  fst <$> filter (uncurry p) depsAlpha
  where
    depsAlpha = map (\k -> (k, Map.lookup k deps)) alphabet
    p k ml = k `notElem` alreadyExecuted && (isNothing ml || ml == Just [])

{-|
  Calculates total execution time. Uses an event priority queue, to keep track
  of what things happen at what times.

  - If no next steps are available, return result time, and executed steps.
  - If there are next steps available, allocate one step to each free available
    worker.
-}
executeParallelSteps :: [StepDependency] -> Int -> Int -> [Step]
                     -> (Int, [Step])
executeParallelSteps stepDeps workerCount baseTime alphabet =
  aux (Map.singleton 0 []) (stepDependencies stepDeps) [] [] workerCount
  where
    aux :: Map Int [Step] -> Map Step [Step] -> [Step] -> [Step] -> Int
        -> (Int, [Step])
    aux q deps executed allocated workerCount' =
      case nextSteps of
        [] -> (t, executed')
        _  -> aux q''
                  deps'
                  executed'
                  (allocated ++ nextWorkerSteps)
                  (workerCount'' - length nextWorkerSteps)
      where
        workerCount'' = workerCount' + length finishedSteps
        ((t, finishedSteps), q') = Map.deleteFindMin q
        deps' = foldl executeStep deps finishedSteps
        executed' = executed ++ finishedSteps
        nextSteps = getNextSteps deps' executed' alphabet
        nextUnallocatedSteps = filter (`notElem` allocated) nextSteps
        nextWorkerSteps = take workerCount'' nextUnallocatedSteps
        q'' = foldl (\acc s ->
                       Map.insertWith (++) (t + stepTime s baseTime) [s] acc)
                    q' nextWorkerSteps

stepTime :: Step -> Int -> Int
stepTime step baseTime = ord step - ord 'A' + 1 + baseTime
