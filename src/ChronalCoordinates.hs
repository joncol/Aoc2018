module ChronalCoordinates where

import           Control.Monad (void)
import           Data.List (elemIndex, group, sort, transpose)
import qualified Data.Map.Strict as Map
import qualified Data.Set as S
import qualified Text.Megaparsec.Char as M
import qualified Text.Megaparsec.Char.Lexer as M

import           Util

type Coord = (Int, Int)

coordParser :: Parser Coord
coordParser = do
  x <- M.decimal
  void $ M.char ',' >> M.space1
  y <- M.decimal
  return (x, y)

largestArea :: [Coord] -> Int
largestArea coords = maximum . map length . group . sort $ kernel
  where
    (xs, ys) = coveringArea coords
    m        = map (\y -> map (\x -> closestCoordIndex coords (x, y)) xs) ys
    tm       = transpose m
    border   = S.fromList $ head m ++ last m ++ head tm ++ last tm
    kernel   = filter (`S.notMember` border) $ concat m

coveringArea :: [Coord] -> ([Int], [Int])
coveringArea coords = ( [minimum xs..maximum xs]
                      , [minimum ys..maximum ys] )
  where
    xs     = map fst coords
    ys     = map snd coords

closestCoordIndex :: [Coord] -> Coord -> Maybe Int
closestCoordIndex coords c
  | null xs = elemIndex x coords
  | otherwise = Nothing
  where
    distList = map (distance c) coords
    distMap = Map.fromListWith (++) (zip distList (map (:[]) coords))
    (_, x:xs) = Map.findMin distMap

distance :: Coord -> Coord -> Int
distance (x1, y1) (x2, y2) = abs (x2 - x1) + abs (y2 - y1)

safeArea :: [Coord] -> Int -> Int
safeArea coords maxDistance = length $ filter (<maxDistance) (concat m)
  where
    (xs, ys) = coveringArea coords
    m        = map (\y -> map (\x -> sumOfDistances coords (x, y)) xs) ys

sumOfDistances :: [Coord] -> Coord -> Int
sumOfDistances coords c = sum $ map (distance c) coords
