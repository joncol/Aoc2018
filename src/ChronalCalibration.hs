module ChronalCalibration where

import           Data.Maybe (fromJust)
import qualified Data.Set as S
import           Text.Megaparsec ((<|>))
import qualified Text.Megaparsec.Char as M
import qualified Text.Megaparsec.Char.Lexer as M

import           Util

newtype Frequency = Frequency Int deriving (Eq, Ord, Show)

data FrequencyChange = Increase Int
                     | Decrease Int
                     deriving (Eq, Show)

applyFrequencyChanges :: [FrequencyChange] -> Frequency
applyFrequencyChanges = foldr applyFrequencyChange (Frequency 0)

frequencyChangeParser :: Parser FrequencyChange
frequencyChangeParser =
  (M.char '+' >> (Increase <$> M.decimal)) <|>
  (M.char '-' >> (Decrease <$> M.decimal))

applyFrequencyChange :: FrequencyChange -> Frequency -> Frequency
applyFrequencyChange change (Frequency x)
  | Increase y <- change = Frequency $ x+y
  | Decrease y <- change = Frequency $ x-y

findFirstRepeatedFrequency :: [FrequencyChange] -> Frequency
findFirstRepeatedFrequency changes =
  fromJust . findFirstRepetition $ freqs
  where freqs = scanl (flip applyFrequencyChange)
                      (Frequency 0)
                      (concat . repeat $ changes)

findFirstRepetition :: Ord a => [a] -> Maybe a
findFirstRepetition xs = aux xs S.empty
  where
    aux :: Ord a => [a] -> S.Set a -> Maybe a
    aux [] _        = Nothing
    aux (y:ys) seen =
      if S.member y seen
        then Just y
        else aux ys (S.insert y seen)
