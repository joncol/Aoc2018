{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module MineCartMadness where

import           Control.Applicative ((<|>))
import           Control.Arrow ((&&&))
import           Control.Lens (over, set, view)
import           Control.Lens.Operators
import           Control.Lens.Tuple (_1, _2)
import           Data.Function (on)
import           Data.Functor (($>))
import           Data.List (find, groupBy, sortOn)
import qualified Data.Text as T
import           Linear.V2 (V2(..), _x, _y)
import           Linear.Vector ((^+^))
import           Text.Megaparsec as M
import           Text.Megaparsec.Char as M

import           Util

data ParsedTrack = PTVertical
                 | PTHorizontal
                 | PTSlashCurve
                 | PTBackSlashCurve
                 | PTIntersection
                 | PTCartUp
                 | PTCartDown
                 | PTCartLeft
                 | PTCartRight
                 | PTEmpty
                 deriving (Eq, Show)

data Track = TrackVertical
           | TrackHorizontal
           | TrackSlashCurve
           | TrackBackSlashCurve
           | TrackIntersection
           | TrackEmpty
           deriving (Eq, Show)

type ParsedTracks = [[ParsedTrack]]

type Tracks = [[Track]]

data Direction = DirUp | DirDown | DirLeft | DirRight deriving (Eq, Show)

data TurnDirection = TurnDirLeft
                   | TurnDirStraight
                   | TurnDirRight
                   deriving (Eq, Show)

data Cart = Cart { cartPosition          :: V2 Int
                 , cartDirection         :: Direction
                 , cartNextTurnDirection :: TurnDirection
                 } deriving (Eq, Show)

directionToV2 :: Direction -> V2 Int
directionToV2 dir = case dir of
                      DirUp    -> V2 0 (-1)
                      DirDown  -> V2 0 1
                      DirLeft  -> V2 (-1) 0
                      DirRight -> V2 1 0

mineCartSetupParser :: Parser ParsedTracks
mineCartSetupParser = trackRowParser `M.sepBy` M.newline

trackRowParser :: Parser [ParsedTrack]
trackRowParser = M.many trackParser

trackParser :: Parser ParsedTrack
trackParser =
  M.char '|' $> PTVertical <|>
  M.char '-' $> PTHorizontal <|>
  M.char '/' $> PTSlashCurve <|>
  M.char '\\' $> PTBackSlashCurve <|>
  M.char '+' $> PTIntersection <|>
  M.char '^' $> PTCartUp <|>
  M.char 'v' $> PTCartDown <|>
  M.char '<' $> PTCartLeft <|>
  M.char '>' $> PTCartRight <|>
  M.char ' ' $> PTEmpty

convertTrack :: ParsedTracks -> Tracks
convertTrack = map (map toTrack)
  where toTrack track =
          case track of
            PTVertical       -> TrackVertical
            PTHorizontal     -> TrackHorizontal
            PTSlashCurve     -> TrackSlashCurve
            PTBackSlashCurve -> TrackBackSlashCurve
            PTIntersection   -> TrackIntersection
            PTCartUp         -> TrackVertical
            PTCartDown       -> TrackVertical
            PTCartLeft       -> TrackHorizontal
            PTCartRight      -> TrackHorizontal
            PTEmpty          -> TrackEmpty

findCarts :: ParsedTracks -> [Cart]
findCarts = fst . foldl (\acc row -> updateCoords (foldl aux acc row)) initVal
  where
    initVal = ([], V2 0 0)
    updateCoords = set (_2._x) 0 . over (_2._y) (+1)
    aux (carts, pos) x = case toCartMaybe x pos of
                           Just c -> (carts ++ [c], pos')
                           Nothing -> (carts, pos')
      where pos' = pos & _x %~ (+1)

sortCarts :: [Cart] -> [Cart]
sortCarts = sortOn ((view _y &&& view _x) . cartPosition)

toCartMaybe :: ParsedTrack -> V2 Int -> Maybe Cart
toCartMaybe pt pos =
    case pt of
      PTCartUp    -> Just $ Cart pos DirUp TurnDirLeft
      PTCartDown  -> Just $ Cart pos DirDown TurnDirLeft
      PTCartLeft  -> Just $ Cart pos DirLeft TurnDirLeft
      PTCartRight -> Just $ Cart pos DirRight TurnDirLeft
      _           -> Nothing

renderTrack :: Tracks -> [Cart] -> T.Text
renderTrack tracks carts =
  fst $ foldl (\acc row -> stepRow (foldl aux acc row)) initVal (take 80 tracks)
  where
    initVal = ("", V2 0 0)
    stepRow = over _1 (`T.snoc` '\n') . set (_2._x) 0 . over (_2._y) (+1)
    aux (result, pos) x = case cart of
                          Just c  -> (T.snoc result $ cartToChar c, pos')
                          Nothing -> (T.snoc result $ trackToChar x, pos')
      where
        pos' = pos & _x %~ (+1)
        cart = find (\c -> cartPosition c == pos) carts

trackToChar :: Track -> Char
trackToChar track = case track of
                      TrackVertical       -> '|'
                      TrackHorizontal     -> '-'
                      TrackSlashCurve     -> '/'
                      TrackBackSlashCurve -> '\\'
                      TrackIntersection   -> '+'
                      TrackEmpty          -> ' '

cartToChar :: Cart -> Char
cartToChar Cart{..} = case cartDirection of
                        DirUp -> '^'
                        DirDown -> 'v'
                        DirLeft -> '<'
                        DirRight -> '>'

findFirstCollision :: Tracks -> [Cart] -> V2 Int
findFirstCollision tracks carts =
  if null collisions
    then findFirstCollision tracks (sortCarts carts')
    else snd (head collisions)
  where (carts', collisions) = moveCarts tracks carts

runUntilOneCartLeft :: Tracks -> [Cart] -> Maybe (V2 Int)
runUntilOneCartLeft tracks carts =
  case length carts of
    0 -> Nothing
    1 -> Just $ cartPosition (head carts)
    _ -> runUntilOneCartLeft tracks (sortCarts remainingCarts)
  where
    (carts', collisions) = moveCarts tracks carts
    collIdxs = map fst collisions
    remainingCarts = map snd . filter (\(i, _) -> i `notElem` collIdxs) .
                           zip [0..] $ carts'

moveCarts :: Tracks -> [Cart] -> ([Cart], [(Int, V2 Int)])
moveCarts tracks carts = foldl aux (carts, []) [0..length carts - 1]
  where aux (carts', collisions) idx = (carts'', collisions ++ colls)
          where
            (l, r) = splitAt idx carts'
            cart = moveCart tracks (carts !! idx)
            carts'' = l ++ [cart] ++ tail r
            colls = findCollisions carts''

moveCart :: Tracks -> Cart -> Cart
moveCart tracks cart@Cart{..} = Cart newPos newDir newNextTurnDir
  where
    newPos = cartPosition ^+^ directionToV2 cartDirection
    nextTrack = (tracks !! (newPos^._y)) !! (newPos^._x)
    newDir = getNewDirection cart nextTrack
    newNextTurnDir = if nextTrack == TrackIntersection
                     then getNextTurnDirection cartNextTurnDirection
                     else cartNextTurnDirection

getNewDirection :: Cart -> Track -> Direction
getNewDirection Cart{..} track = turn cartDirection turnDir
  where turnDir = getTurnDirection cartDirection track cartNextTurnDirection

getTurnDirection :: Direction -> Track -> TurnDirection -> TurnDirection
getTurnDirection prevDir track nextTurnDir
  | vert prevDir && track == TrackSlashCurve ||
    horiz prevDir && track == TrackBackSlashCurve = TurnDirRight
  | horiz prevDir && track == TrackSlashCurve ||
    vert prevDir && track == TrackBackSlashCurve = TurnDirLeft
  | track == TrackIntersection = nextTurnDir
  | otherwise = TurnDirStraight
  where
    vert dir = dir == DirUp || dir == DirDown
    horiz dir = dir == DirLeft || dir == DirRight

getNextTurnDirection :: TurnDirection -> TurnDirection
getNextTurnDirection dir =
  case dir of
    TurnDirLeft     -> TurnDirStraight
    TurnDirStraight -> TurnDirRight
    TurnDirRight    -> TurnDirLeft

turn :: Direction -> TurnDirection -> Direction
turn dir turnDir =
  case turnDir of
    TurnDirRight -> case dir of
      DirUp    -> DirRight
      DirDown  -> DirLeft
      DirRight -> DirDown
      DirLeft  -> DirUp
    TurnDirLeft -> case dir of
      DirUp    -> DirLeft
      DirDown  -> DirRight
      DirRight -> DirUp
      DirLeft  -> DirDown
    TurnDirStraight -> dir

findCollisions :: [Cart] -> [(Int, V2 Int)]
findCollisions carts = concat . filter ((>1) . length) $ grouped
  where
    indexed = zip [(0::Int)..] (map cartPosition carts)
    sorted = sortOn snd indexed
    grouped = groupBy ((==) `on` snd) sorted
