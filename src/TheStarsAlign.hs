{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module TheStarsAlign where

import           Prelude hiding ((++))
import           Control.Arrow ((&&&))
import           Control.Lens.Getter (view)
import           Data.List (elemIndex)
import           Data.Maybe (fromJust)
import qualified Data.Text as T
import           Linear.V2 (V2(..), _x, _y)
import           Linear.Vector ((^+^))

import           Util

data Star = Star { starPosition :: V2 Int
                 , starVelocity :: V2 Int
                 } deriving (Eq, Show)

vector2Parser :: Parser (V2 Int)
vector2Parser =
  angleBrackets $ V2 <$> signedInteger <* symbol "," <*> signedInteger

starParser :: Parser Star
starParser = do
  starPosition <- symbol "position=" *> vector2Parser
  starVelocity <- symbol "velocity=" *> vector2Parser
  return Star{..}

renderAlignedStars :: [Star] -> (T.Text, Int)
renderAlignedStars stars = (renderStars alignedStars, idx)
  where (alignedStars, idx) = alignStars stars

renderStars :: [Star] -> T.Text
renderStars stars = T.intercalate "\n" $ map renderLine grid
  where
    (xMin, yMin, xMax, yMax) = starsExtent stars
    grid = [[V2 x y | x <- [xMin..xMax]] | y <- [yMin..yMax]]
    starPositions = map starPosition stars
    renderLine = T.pack . map (\p -> if p `elem` starPositions
                                     then '#'
                                     else ' ')

alignStars :: [Star] -> ([Star], Int)
alignStars stars = (movedStars !! idx, idx)
  where
    movedStars = iterate (map moveStar) stars
    areas = map starsArea movedStars
    minArea = fromJust $ localMinimum areas
    idx = fromJust $ elemIndex minArea areas

moveStar :: Star -> Star
moveStar Star{..} = Star (starPosition ^+^ starVelocity) starVelocity

starsArea :: [Star] -> Int
starsArea stars = (xMax - xMin) * (yMax - yMin)
  where (xMin, yMin, xMax, yMax) = starsExtent stars

starsExtent :: [Star] -> (Int, Int, Int, Int)
starsExtent stars = (xMin, yMin, xMax, yMax)
  where
    xs = map (view _x . starPosition) stars
    ys = map (view _y . starPosition) stars
    (xMin, xMax) = (minimum &&& maximum) xs
    (yMin, yMax) = (minimum &&& maximum) ys

localMinimum :: Ord a => [a] -> Maybe a
localMinimum (x:rest@(y:z:_))
  | y < x && y < z = Just y
  | otherwise      = localMinimum rest
localMinimum _ = Nothing
