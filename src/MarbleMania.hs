{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module MarbleMania where

import           Prelude hiding ((++))
import           Control.Lens.Operators
import           Control.Monad (void)
import           Data.List.PointedList.Circular (PointedList)
import qualified Data.List.PointedList.Circular as C
import           Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import           Data.Maybe (fromJust)
import qualified Text.Megaparsec.Char as M

import           Util

type Marble = Int
type Score = Int
type Player = Int

type MarbleList = PointedList Marble

data GameSetup = GameSetup { gsPlayerCount :: Int
                           , gsLastMarble  :: Marble
                           } deriving (Eq, Show)

gameSetupParser :: Parser GameSetup
gameSetupParser = do
  gsPlayerCount <- integer
  void $ M.string "players; last marble is worth "
  gsLastMarble <- integer
  void $ M.string "points"
  return GameSetup{..}

placeMarble :: MarbleList -> Marble -> (MarbleList, Score)
placeMarble marbles nextMarble
  | nextMarble `mod` 23 /= 0 =
    let marbles'  = C.moveN 1 marbles
    in (C.insert nextMarble marbles', 0)
  | otherwise =
    let marbles' = C.moveN (-7) marbles
        score    = nextMarble + (marbles' ^. C.focus)
    in (fromJust $ C.delete marbles', score)

winningScore :: GameSetup -> Int
winningScore GameSetup{..} =
  let (_, scores, _) = foldl aux initVal [1..gsLastMarble]
  in maximum scores
  where
    initVal = (C.singleton 0, Map.empty, 1)
    aux :: (MarbleList, Map Player Score, Player) -> Marble
        -> (MarbleList, Map Player Score, Player)
    aux (marbles, scores, player) nextMarble =
      let (marbles', score) = placeMarble marbles nextMarble
          scores' = Map.insertWith (+) player score scores
      in (marbles', scores', (player + 1) `mod` gsPlayerCount)
