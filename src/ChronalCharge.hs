-- Algorithm idea from:
-- https://www.geeksforgeeks.org/print-maximum-sum-square-sub-matrix-of-given-size

module ChronalCharge where

import Control.Arrow ((&&&))
import Control.Lens (view)
import Control.Lens.Tuple (_1, _3)
import Data.Function (on)
import Data.List (elemIndex, maximumBy, transpose)
import Data.Maybe (fromJust)

getPowerLevel :: Int -> (Int, Int) -> Int
getPowerLevel gridSerialNumber (x, y) =
  getHundreds ((rackId * y + gridSerialNumber) * rackId) - 5
  where
    rackId = x + 10
    getHundreds n = (n `div` 100) `mod` 10

getLargestTotalPowerSquarePosition :: Int -> Int -> Int -> (Int, (Int, Int))
getLargestTotalPowerSquarePosition gridSerialNumber gridSize k =
  getLargestSumSquareSubMatrix k levels
  where
    grid = [[(x, y) | y <- [1..gridSize]] | x <- [1..gridSize]]
    levels = map (map $ getPowerLevel gridSerialNumber) grid

getLargestSumSquareSubMatrix :: (Num a, Ord a) => Int -> [[a]]
                             -> (a, (Int, Int))
getLargestSumSquareSubMatrix k m = (maxSum, (row+1, col+1))
  where
    maxSums = map (subListMaxSum k) $ getStripSumMatrix k m
    x@(maxSum, col) = maximumBy (compare `on` fst) maxSums
    row = fromJust $ elemIndex x maxSums

getStripSumMatrix :: Num a => Int -> [[a]] -> [[a]]
getStripSumMatrix k = transpose . map (subListSums k) . transpose

subListSums :: Num a => Int -> [a] -> [a]
subListSums n xs = map (view _1) $ scanl aux initVal (drop n xs)
  where
    initVal = (sum . take n $ xs, xs)
    aux (sum', y:ys) x = (sum' - y + x, ys)
    aux (_, []) _ = undefined

subListMaxSum :: (Num a, Ord a) => Int -> [a] -> (a, Int)
subListMaxSum n xs =
  maximum . map (view _1 &&& view _3) . scanl aux initVal $ drop n xs
  where
    initVal = (sum . take n $ xs, xs, 0)
    aux (sum', y:ys, idx) x = (sum'-y+x, ys, idx+1)
    aux (_, [], _) _ = error "This shouldn't happen"
