module AlchemicalReduction where

import           Data.Char (toLower)
import           Data.List (filter)
import qualified Data.Set as Set

reducePolymer :: String -> String
reducePolymer = foldr aux ""
  where
    aux x (y:ys) | x /= y && toLower x == toLower y = ys
    aux x ys = x : ys

removeProblematicUnit :: String -> Int
removeProblematicUnit s =
  Set.findMin $ Set.map (length . reducePolymer . removeUnit s) units
  where units = Set.fromList $ map toLower s

removeUnit :: String -> Char -> String
removeUnit s u = filter (\c -> toLower c /= u) s
