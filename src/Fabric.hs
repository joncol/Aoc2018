{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Fabric where

import           Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set
import qualified Text.Megaparsec.Char as M
import qualified Text.Megaparsec.Char.Lexer as M

import           Util

newtype ClaimId = ClaimId Int deriving (Eq, Ord, Show)

data Claim = Claim { claimId     :: ClaimId
                   , claimX      :: Int
                   , claimY      :: Int
                   , claimWidth  :: Int
                   , claimHeight :: Int
                   } deriving (Eq, Show)

overlappingSquareInches :: [Claim] -> Int
overlappingSquareInches claims =
  Map.size . Map.filter ((>=2) . length) $ fabric
  where fabric = foldr paintClaim Map.empty claims

nonOverlappingClaim :: [Claim] -> ClaimId
nonOverlappingClaim claims =
  head . Set.toList $ Set.difference singles overlapping
  where
    fabric = foldr paintClaim Map.empty claims
    singles = Set.fromList . concat . Map.elems . Map.filter ((==1) . length) $
                fabric
    overlapping = Set.fromList . concat . Map.elems .
                    Map.filter ((>=2) . length) $ fabric

claimParser :: Parser Claim
claimParser = do
  claimId <- M.char '#' >> ClaimId <$> M.decimal
  _ <- M.string " @ "
  claimX <- M.decimal
  _ <- M.char ','
  claimY <- M.decimal
  _ <- M.string ": "
  claimWidth <- M.decimal
  _ <- M.char 'x'
  claimHeight <- M.decimal
  return Claim{..}

paintClaim :: Claim -> Map (Int, Int) [ClaimId] -> Map (Int, Int) [ClaimId]
paintClaim Claim{..} fabric =
  foldr (\y fabric' ->
    foldr (\x fabric'' -> Map.insertWith (++) (x, y) [claimId] fabric'')
          fabric'
          [claimX..(claimX + claimWidth - 1)])
    fabric
    [claimY..(claimY + claimHeight - 1)]
