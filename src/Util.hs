{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeFamilies #-}

module Util where

import           Control.Applicative (empty)
import           Data.String (IsString)
import qualified Data.Text as T
import qualified Data.Text.IO as TIO
import           Data.Void (Void)
import qualified Text.Megaparsec as M
import qualified Text.Megaparsec.Char as M
import qualified Text.Megaparsec.Char.Lexer as L

type Parser = M.Parsec Void T.Text
type ParseErrorBundle = M.ParseErrorBundle T.Text Void

readContents :: FilePath -> IO T.Text
readContents = TIO.readFile

spaceConsumer :: (M.MonadParsec e s m, M.Token s ~ Char) => m ()
spaceConsumer = L.space M.space1 empty empty

lexeme :: (M.MonadParsec e s m, M.Token s ~ Char) => m a -> m a
lexeme = L.lexeme spaceConsumer

symbol :: (M.MonadParsec e s m, M.Token s ~ Char)
       => M.Tokens s -> m (M.Tokens s)
symbol = L.symbol spaceConsumer

integer :: (M.MonadParsec e s m, M.Token s ~ Char, Integral a) => m a
integer = lexeme L.decimal

signedInteger :: (M.MonadParsec e s m, M.Token s ~ Char, Integral a) => m a
signedInteger = L.signed spaceConsumer integer

parens :: (M.MonadParsec e s m, M.Token s ~ Char, IsString (M.Tokens s))
       => m a -> m a
parens = M.between (symbol "(") (symbol ")")

brackets :: (M.MonadParsec e s m, M.Token s ~ Char, IsString (M.Tokens s))
         => m a -> m a
brackets = M.between (symbol "[") (symbol "]")

angleBrackets :: (M.MonadParsec e s m, M.Token s ~ Char, IsString (M.Tokens s))
              => m a -> m a
angleBrackets = M.between (symbol "<") (symbol ">")
