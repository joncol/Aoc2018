{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module ChronalClassification where

import           Control.Arrow ((>>>), (&&&))
import           Control.Lens (ix, makeLenses, over, view)
import           Control.Lens.Operators
import           Control.Lens.Tuple (_1, _2)
import           Data.Bits ((.&.), (.|.))
import           Data.Function ((&))
import           Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import           Data.Set (Set)
import qualified Data.Set as Set
import qualified Data.Text as T
import qualified Text.Megaparsec as M
import           Text.Printf (printf)

import           Util

data Registers = Registers { _reg0 :: Int
                           , _reg1 :: Int
                           , _reg2 :: Int
                           , _reg3 :: Int
                           } deriving Eq

makeLenses ''Registers

instance Show Registers
  where show Registers{..} = printf "%d %d %d %d" _reg0 _reg1 _reg2 _reg3

data Instruction = Instruction { _instrOpcode :: Int
                               , _instrA      :: Int
                               , _instrB      :: Int
                               , _instrC      :: Int
                               } deriving Eq

makeLenses ''Instruction

instance Show Instruction
  where show Instruction{..} =
          printf "%d %d %d %d" _instrOpcode _instrA _instrB _instrC

data Sample = Sample { _sampleBefore      :: Registers
                     , _sampleInstruction :: Instruction
                     , _sampleAfter       :: Registers
                     } deriving (Eq, Show)

makeLenses ''Sample

type OpName = T.Text
type OpFun = Int -> Int -> Int -> Registers -> Registers
type OpcodeTable = Map Int OpName
type BinaryOp = Int -> Int -> Int

operations :: Map OpName OpFun
operations =
  Map.fromList [ ("addr", rrInstruction (+))
               , ("addi", riInstruction (+))
               , ("mulr", rrInstruction (*))
               , ("muli", riInstruction (*))
               , ("banr", rrInstruction (.&.))
               , ("bani", riInstruction (.&.))
               , ("borr", rrInstruction (.|.))
               , ("bori", riInstruction (.|.))
               , ("setr", rrInstruction const)
               , ("seti", iiInstruction const)
               , ("gtir", irInstruction (\a b -> if a > b then 1 else 0))
               , ("gtri", riInstruction (\a b -> if a > b then 1 else 0))
               , ("gtrr", rrInstruction (\a b -> if a > b then 1 else 0))
               , ("eqir", irInstruction (\a b -> if a == b then 1 else 0))
               , ("eqri", riInstruction (\a b -> if a == b then 1 else 0))
               , ("eqrr", rrInstruction (\a b -> if a == b then 1 else 0))
               ]

inputParser :: Parser ([Sample], [Instruction])
inputParser =
  do
    samples <- M.many sampleParser
    instructions <- M.many instructionParser
    return (samples, instructions)

sampleParser :: Parser Sample
sampleParser =
  do
    _sampleBefore <- symbol "Before:" *> registersParser
    _sampleInstruction <- instructionParser
    _sampleAfter <- symbol "After:" *> registersParser
    return Sample{..}

registersParser :: Parser Registers
registersParser =
  do
    [_reg0, _reg1, _reg2, _reg3] <- brackets $ M.sepBy integer (symbol ",")
    return Registers{..}

instructionParser :: Parser Instruction
instructionParser =
  do
    [_instrOpcode, _instrA, _instrB, _instrC] <- M.count 4 integer
    return Instruction{..}

getReg :: Registers -> Int -> Int
getReg regs n
  | n == 0 = regs ^. reg0
  | n == 1 = regs ^. reg1
  | n == 2 = regs ^. reg2
  | n == 3 = regs ^. reg3
  | otherwise = error "Index out of range"

setReg :: Registers -> Int -> Int -> Registers
setReg regs n value
  | n == 0 = regs & reg0 .~ value
  | n == 1 = regs & reg1 .~ value
  | n == 2 = regs & reg2 .~ value
  | n == 3 = regs & reg3 .~ value
  | otherwise = error "Index out of range"

rrInstruction :: BinaryOp -> OpFun
rrInstruction op inputA inputB outputC regs =
  setReg regs outputC (a `op` b)
  where
    a = getReg regs inputA
    b = getReg regs inputB

riInstruction :: BinaryOp -> OpFun
riInstruction op inputA inputB outputC regs =
  setReg regs outputC (a `op` inputB)
  where a = getReg regs inputA

irInstruction :: BinaryOp -> OpFun
irInstruction op inputA inputB outputC regs =
  setReg regs outputC (inputA `op` b)
  where b = getReg regs inputB

iiInstruction :: BinaryOp -> OpFun
iiInstruction op inputA inputB outputC regs =
  setReg regs outputC (inputA `op` inputB)

getMultiSampleCount :: [Sample] -> Int
getMultiSampleCount =
  length . filter ((>=3) . Set.size . getCompatibleOperations)

findOpcodes :: [Sample] -> OpcodeTable
findOpcodes samples = go 16 Map.empty
  where
    go :: Int -> OpcodeTable -> OpcodeTable
    go n opcodesFound
      | n == 0    = opcodesFound
      | otherwise = go (n-1) $ Map.union opcodes opcodesFound
      where
        found = Set.fromList $ Map.elems opcodesFound
        ops = samples & (map (id &&& getCompatibleOperations)
                     >>> map (over _1 (Set.singleton . view (sampleInstruction .
                                                             instrOpcode))))
        ops' = map (over _2 (`Set.difference` found)) ops
        unique = filter ((==1) . Set.size . view _2) ops'
        unique' = map (over _1 (Set.elemAt 0) .
                       over _2 (Set.elemAt 0)) unique
        opcodes = Map.fromList unique'

getCompatibleOperations :: Sample -> Set OpName
getCompatibleOperations sample =
  Map.keysSet $ Map.filter (\f -> f a b c regsBefore == regsAfter) operations
  where
    regsBefore = sample ^. sampleBefore
    regsAfter  = sample ^. sampleAfter
    a          = sample ^. sampleInstruction . instrA
    b          = sample ^. sampleInstruction . instrB
    c          = sample ^. sampleInstruction . instrC

runInstructions :: OpcodeTable -> [Instruction] -> Registers
runInstructions opcodes = foldl go (Registers 0 0 0 0)
  where
    go regs instr =
      let
        opcode = (instr ^. instrOpcode)
        opname = (opcodes ^?! ix opcode)
        f = operations ^?! ix opname
        a = instr ^. instrA
        b = instr ^. instrB
        c = instr ^. instrC
      in f a b c regs
