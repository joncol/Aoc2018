{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TemplateHaskell #-}

module SettlersOfTheNorthPole where

import           Control.Applicative ((<|>))
import           Control.Lens (Prism', makePrisms, preview)
import           Control.Lens.Operators
import           Data.Functor (($>))
import           Data.Maybe (fromJust, mapMaybe)
import           Data.List (elemIndex)
import qualified Data.Text as T
import           Linear.V2 (_x, _y, V2(..))
import qualified Text.Megaparsec as M
import qualified Text.Megaparsec.Char as M

import           Util

data Acre = Open | Trees | Lumberyard deriving (Eq, Show)

makePrisms ''Acre

type Area = [[Acre]]

acreParser :: Parser Acre
acreParser =
  M.char '.' $> Open <|>
  M.char '|' $> Trees <|>
  M.char '#' $> Lumberyard

areaParser :: Parser Area
areaParser = filter (not . null) <$> M.many acreParser `M.sepBy` M.eol

getResourceValue :: Area -> Int
getResourceValue area = typeCount acres _Trees * typeCount acres _Lumberyard
  where acres = concat area

typeCount :: [a] -> Prism' a b -> Int
typeCount xs t = length . mapMaybe (preview t) $ xs

getAdjacentPositions :: Int -> Int -> V2 Int -> [V2 Int]
getAdjacentPositions width height pos =
  filter (\(V2 x y) -> 0 <= x && x < width && 0 <= y && y < height) ps
  where ps = [pos & _x +~ xo
                  & _y +~ yo | yo <- [-1..1]
                             , xo <- [-1..1]
                             , not $ xo == 0 && yo == 0]

getAdjacentAcres :: Area -> V2 Int -> [Acre]
getAdjacentAcres area pos = map (\(V2 x y) -> (area !! y) !! x) ps
  where
    w = length $ head area
    h = length area
    ps = getAdjacentPositions w h pos

evolveArea :: Area -> [Area]
evolveArea = iterate stepArea

stepArea :: Area -> Area
stepArea area = map (\y -> map (\x -> stepAcre area (V2 x y)) [0..w-1]) [0..h-1]
  where
    w = length $ head area
    h = length area

stepAcre :: Area -> V2 Int -> Acre
stepAcre area pos@(V2 x y) =
  case old of
    Open | typeCount adj _Trees >= 3 -> Trees
         | otherwise -> Open
    Trees | typeCount adj _Lumberyard >= 3 -> Lumberyard
          | otherwise -> Trees
    Lumberyard | typeCount adj _Lumberyard >= 1 &&
                 typeCount adj _Trees >= 1 -> Lumberyard
               | otherwise -> Open
  where
    adj = getAdjacentAcres area pos
    old = (area !! y) !! x

renderArea :: Area -> T.Text
renderArea area =
  map (\y -> T.pack $ map (\x -> acreToChar $ (area !! y) !! x) [0..w-1])
      [0..h-1] & T.intercalate "\n"
  where
    w = length $ head area
    h = length area
    acreToChar Open = '.'
    acreToChar Trees = '|'
    acreToChar Lumberyard = '#'

getFutureResourceValue :: Area -> Int -> Int
getFutureResourceValue area mins = xs !! idx
  where
    offset = 500
    xs = map getResourceValue (drop offset $ evolveArea area)
    period = (fromJust . elemIndex (head xs) $ tail xs) + 1
    idx = (mins - offset) `mod` period
