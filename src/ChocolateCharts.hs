{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE ViewPatterns #-}

module ChocolateCharts
where

import           Data.List (isPrefixOf, tails)
import           Data.Sequence ((><))
import qualified Data.Sequence as Seq

recipes :: [Int]
recipes = 3 : 7 : go 0 1 (Seq.fromList [3, 7])
  where
    go !i !j !tp = new ++ go i' j' tp'
      where
        x = Seq.index tp i
        y = Seq.index tp j
        new = get2Digits $ x + y
        tp' = tp >< Seq.fromList new
        i' = (i + x + 1) `mod` Seq.length tp'
        j' = (j + y + 1) `mod` Seq.length tp'

get2Digits :: Int -> [Int]
get2Digits ((`divMod` 10)->(x,y))
  | x == 0 = [y]
  | otherwise = [x, y]

findSubListIndex :: [Int] -> [Int] -> Int
findSubListIndex xs = length . takeWhile (not . (xs `isPrefixOf`)) . tails

getDigits :: Int -> [Int]
getDigits 0 = [0]
getDigits n = reverse . go $ n
  where
    go ((`divMod` 10)->(x,y))
      | x == 0 = [y]
      | otherwise = y : go x
