{-# LANGUAGE RecordWildCards #-}

module MemoryManeuver where

import qualified Text.Megaparsec as M

import           Util

data TreeNode = TreeNode { treeNodeHeader          :: Header
                         , treeNodeChildren        :: [TreeNode]
                         , treeNodeMetadataEntries :: [Int]
                         } deriving (Eq, Show)

data Header = Header { headerChildCount         :: Int
                     , headerMetadataEntryCount :: Int
                     } deriving (Eq, Show)

emptyTree :: TreeNode
emptyTree = TreeNode (Header 0 0) [] []

headerParser :: Parser Header
headerParser = do
  headerChildCount <- integer
  headerMetadataEntryCount <- integer
  return Header{..}

treeNodeParser :: Parser TreeNode
treeNodeParser = do
  header <- headerParser
  children <- M.count (headerChildCount header) treeNodeParser
  metadataEntries <- M.count (headerMetadataEntryCount header) integer
  return TreeNode { treeNodeHeader = header
                  , treeNodeChildren = children
                  , treeNodeMetadataEntries = metadataEntries
                  }

allMetadataEntries :: TreeNode -> [Int]
allMetadataEntries TreeNode{..} =
  treeNodeMetadataEntries ++ concatMap allMetadataEntries treeNodeChildren

getNodeValue :: TreeNode -> Int
getNodeValue (TreeNode _ [] meta) = sum meta
getNodeValue (TreeNode _ _ []) = 0
getNodeValue (TreeNode header children (m:ms))
  | 1 <= m && m <= length children = getNodeValue (children !! (m - 1)) +
                                     getNodeValue (TreeNode header children ms)
  | otherwise = getNodeValue (TreeNode header children ms)
