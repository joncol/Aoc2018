{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module ReservoirResearch where

import           Control.Applicative ((<|>))
import           Control.Lens (ix, makeLenses, makePrisms)
import           Control.Lens.Operators
import           Control.Monad (void)
import           Data.Function ((&))
import qualified Data.Map as Map
import           Data.Set (Set)
import qualified Data.Set as Set
import qualified Data.Text as T
import           Linear.V2 (_x, _y, V2(..))
import           Text.Megaparsec as M
import           Text.Printf (printf)

import           Util

data Range = Range { _rangeLowerBound :: Int
                   , _rangeUpperBound :: Int
                   } deriving Eq

instance Show Range
  where show Range{..} = printf "%d..%d" _rangeLowerBound _rangeUpperBound

makeLenses ''Range

data HorizontalSegment = HorizontalSegment { _hsX :: Range
                                           , _hsY :: Int
                                           } deriving (Eq, Show)

makeLenses ''HorizontalSegment

data VerticalSegment = VerticalSegment { _vsX :: Int
                                       , _vsY :: Range
                                       } deriving (Eq, Show)

makeLenses ''VerticalSegment

data Segment = Horizontal HorizontalSegment
             | Vertical VerticalSegment
             deriving (Eq, Show)

makePrisms ''Segment

rangeParser :: Parser Range
rangeParser =
  do
    _rangeLowerBound <- integer
    void $ symbol ".."
    _rangeUpperBound <- integer
    return Range{..}

horizontalSegmentParser :: Parser HorizontalSegment
horizontalSegmentParser =
  do
    _hsY <- symbol "y=" *> integer <* symbol ","
    _hsX <- symbol "x=" *> rangeParser
    return HorizontalSegment{..}

verticalSegmentParser :: Parser VerticalSegment
verticalSegmentParser =
  do
    _vsX <- symbol "x=" *> integer <* symbol ","
    _vsY <- symbol "y=" *> rangeParser
    return VerticalSegment{..}

segmentParser :: Parser Segment
segmentParser = (Horizontal <$> horizontalSegmentParser) <|>
                (Vertical <$> verticalSegmentParser)

segmentsParser :: Parser [Segment]
segmentsParser = M.many segmentParser

getBounds :: Foldable t => t Segment -> (Int, Int, Int, Int)
getBounds = foldr go (maxBound, maxBound, minBound, minBound)
  where
    go segment (xMin, yMin, xMax, yMax) =
      case segment of
        Horizontal HorizontalSegment{..} ->
          let xMin' = min xMin (_rangeLowerBound _hsX)
              yMin' = min yMin _hsY
              xMax' = max xMax (_rangeUpperBound _hsX)
              yMax' = max yMax _hsY
          in (xMin', yMin', xMax', yMax')
        Vertical VerticalSegment{..} ->
          let xMin' = min xMin _vsX
              yMin' = min yMin (_rangeLowerBound _vsY)
              xMax' = max xMax _vsX
              yMax' = max yMax (_rangeUpperBound _vsY)
          in (xMin', yMin', xMax', yMax')

inRange :: Int -> Range -> Bool
inRange x Range{..} = _rangeLowerBound <= x && x <= _rangeUpperBound

intersects :: V2 Int -> Segment -> Bool
intersects (V2 x y) segment
  | Horizontal HorizontalSegment{..} <- segment
  , _hsY == y && x `inRange` _hsX
  = True
  | Vertical VerticalSegment{..} <- segment
  , _vsX == x && y `inRange` _vsY
  = True
  | otherwise = False

isClay :: Foldable t => t Segment -> V2 Int -> Bool
isClay segments xy = any (intersects xy) segments

floodFill :: Foldable t => t Segment -> (Set (V2 Int), Set (V2 Int))
floodFill segments =
  go [startPos] (Set.singleton startPos) Set.empty [] Map.empty False False
  where
    (_, yMin, _, yMax) = getBounds segments

    startPos = V2 500 yMin

    go [] reached filled _ _ _ _ = (reached, filled)
    go (p@(V2 px py):ps) reached filled branches sideWalls branch backtrack
      -- Flowed out of bounds, backtrack to latest unvisited branch point. If
      -- there are no unvisited branch points left, we're done.
      | py > yMax
      = if Set.null $ Set.fromList branches `Set.difference` reached
        then (reached, filled)
        else go ps reached filled branches sideWalls False True

      -- If we're backtracking, stop when current point is equal to the latest
      -- unvisited branch point.
      | backtrack
      = if p `elem` branches && not (isReached p)
        then go (p:ps) reached filled branches sideWalls False False
        else go ps reached filled branches sideWalls False True

      -- Create branch point. We're now at the left of a "waterfall". We came
      -- here from filling with water below. The head of `ps` should be the
      -- actual branch point. It's to the right of the waterfall. Only create an
      -- actual branch point if that position is empty.
      | branch
      = let branches' = if isEmpty (head ps)
                        then head ps:branches
                        else branches
        in go (p:ps) reached filled branches' sideWalls False backtrack

      -- Entered wall. Note that we shouldn't add walls to the `reached` set,
      -- since walls can be double-sided.
      | isClay segments p
      = let sideWalls' = Map.insert py px sideWalls
        in case sideWalls ^? ix py of
             Nothing -> go ps reached filled branches sideWalls' False backtrack
             Just x ->
               -- We need to check if we came here from the left, since we could
               -- have entered another basin during backtracking
               if isReached (p & _x -~ 1)
               then go ps reached (fillFrom x) branches
                       (Map.delete py sideWalls) True backtrack
               else go ps reached filled branches sideWalls' False backtrack

      -- If the below block and the block to the right are flowing water (empty
      -- and visited), then we're in the middle of backtracking and we should
      -- continue doing that. The position we've reached is to the left of a
      -- waterfall, and we shouldn't continue flowing from here, since we've
      -- already been down that (left) path.
      | isEmpty (p & _y +~ 1) && all isReached [p & _y +~ 1, p & _x +~ 1]
      = go ps reached filled branches (Map.delete (py+1) sideWalls) False True

      -- Normal step
      | not (null around)
      = go (around ++ ps) reached' filled branches sideWalls False False

      -- Reached dead end, backtrack to latest unvisited branch point. If there
      -- are no unvisited branch points left, we're done.
      | otherwise
      = if Set.null $ Set.fromList branches `Set.difference` reached
        then (reached, filled)
        else go ps reached filled branches sideWalls False True

      where
        fillFrom x = foldr (\x' acc -> Set.insert (V2 x' py) acc)
                           filled [x+1..px-1]
        reached' = Set.insert p reached
        around = filter isEmpty [p & _y +~ 1] ++
                 filter (not . isReached) [p & _x -~ 1, p & _x +~ 1]
        isEmpty pos = not (isClay segments pos || pos `Set.member` filled)
        isReached = flip Set.member reached

renderReservoir :: Foldable t => t Segment -> T.Text
renderReservoir segments =
  renderSpring `T.append`
    (map (\y -> T.pack $ map (\x -> renderPos $ V2 x y) [x1-1..x2+1])
         [y1..y2] & T.intercalate "\n") `T.append`
    T.pack (printf "\n\nReached count: %d" $ Set.size reached) `T.append`
    T.pack (printf "\nFilled count: %d" $ Set.size filled)
  where
    (x1, y1, x2, y2) = getBounds segments
    (reached, filled) = floodFill segments
    renderSpring =
      T.pack $ map (\x -> if x == 500 then '+' else '.') [x1-1..x2+1] ++ "\n"

    renderPos xy
      | isClay segments xy = '#'
      | xy `Set.member` filled = '~'
      | xy `Set.member` reached = '|'
      | otherwise = '.'
