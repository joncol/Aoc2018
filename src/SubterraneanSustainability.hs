{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module SubterraneanSustainability where

import           Control.Applicative ((<|>))
import           Control.Monad (void)
import           Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import           Data.Maybe (fromMaybe)
import           Safe (headMay)
import           Text.Megaparsec as M
import           Text.Megaparsec.Char as M

import           Util

data PlantSetup = PlantSetup { psInitialState :: String
                             , psRules        :: [Rule]
                 } deriving (Eq, Show)

data Rule = Rule { ruleInput  :: String
                 , ruleOutput :: Char
                 } deriving (Eq, Show)

plantSetupParser :: Parser PlantSetup
plantSetupParser =
  do
    psInitialState <- initialStateParser
    psRules <- M.many ruleParser
    return PlantSetup{..}

initialStateParser :: Parser String
initialStateParser =
  do
    void $ symbol "initial state:"
    M.many plantState <* M.space

plantState :: Parser Char
plantState = M.char '#' <|> M.char '.'

ruleParser :: Parser Rule
ruleParser =
  do
    ruleInput <- M.many plantState <* M.space
    void $ symbol "=>"
    ruleOutput <- plantState <* M.space
    return Rule{..}

sumOfPlantContainingPotIndices :: Map Int Char -> Int
sumOfPlantContainingPotIndices plants = sum keys
  where keys = Map.keys . Map.filter (== '#') $ plants

evolvePlants :: PlantSetup -> [Map Int Char]
evolvePlants PlantSetup{..} =
  iterate (applyRules psRules) plants
  where plants = plantStringToMap psInitialState 0

plantStringToMap :: String -> Int -> Map Int Char
plantStringToMap s offset = foldr (uncurry $ flip Map.insert) Map.empty hasPlants
  where hasPlants = filter ((=='#') . fst) $ zip s [offset..]

applyRules :: [Rule] -> Map Int Char -> Map Int Char
applyRules rules plants = foldl aux Map.empty [minIdx..maxIdx]
  where
    minIdx = (fst . Map.findMin $ Map.filter (== '#') plants) - 2
    maxIdx = (fst . Map.findMax $ Map.filter (== '#') plants) + 2
    aux plants' idx = case matchingRules of
                        Nothing -> plants'
                        Just Rule{..} -> Map.insert idx ruleOutput plants'
      where
        surr = getPotSurroundings idx plants
        matchingRules = headMay . filter ((surr==) . ruleInput) $ rules

getPotSurroundings :: Int -> Map Int Char -> String
getPotSurroundings idx plants =
  map (fromMaybe '.' . flip Map.lookup plants) [idx-2..idx+2]

longEvolution :: Int -> Int -> [Int] -> Int
longEvolution capOffCount generationCount sums =
  (sums !! capOffCount) + (generationCount - capOffCount) * delta
  where delta = last $ take capOffCount $ zipWith (-) (tail sums) sums
