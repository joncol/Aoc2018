{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module ChronalConversion where

import           Control.Lens (ix, makeLenses)
import           Control.Lens.Operators
import           Data.Bits ((.&.), (.|.))
import           Data.Function ((&))
import           Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import qualified Data.Text as T
import           Debug.Trace (trace)
import qualified Text.Megaparsec as M
import qualified Text.Megaparsec.Char as M
import           Text.Printf (printf)

import           Util

data Registers = Registers { _reg0 :: Int
                           , _reg1 :: Int
                           , _reg2 :: Int
                           , _reg3 :: Int
                           , _reg4 :: Int
                           , _reg5 :: Int
                           } deriving Eq

makeLenses ''Registers

instance Show Registers
  where
    show Registers{..} =
      printf "%d %d %d %d %d %d" _reg0 _reg1 _reg2 _reg3 _reg4 _reg5

type OpName = T.Text

data Instruction = Instruction { _instrName :: OpName
                               , _instrA    :: Int
                               , _instrB    :: Int
                               , _instrC    :: Int
                               } deriving Eq

type IpRegister = Int

makeLenses ''Instruction

instance Show Instruction
  where show Instruction{..} =
          printf "%s %d %d %d" _instrName _instrA _instrB _instrC

type OpFun = Int -> Int -> Int -> Registers -> Registers
type BinaryOp = Int -> Int -> Int

operations :: Map OpName OpFun
operations =
  Map.fromList [ ("addr", rrInstruction (+))
               , ("addi", riInstruction (+))
               , ("mulr", rrInstruction (*))
               , ("muli", riInstruction (*))
               , ("banr", rrInstruction (.&.))
               , ("bani", riInstruction (.&.))
               , ("borr", rrInstruction (.|.))
               , ("bori", riInstruction (.|.))
               , ("setr", rrInstruction const)
               , ("seti", iiInstruction const)
               , ("gtir", irInstruction (\a b -> if a > b then 1 else 0))
               , ("gtri", riInstruction (\a b -> if a > b then 1 else 0))
               , ("gtrr", rrInstruction (\a b -> if a > b then 1 else 0))
               , ("eqir", irInstruction (\a b -> if a == b then 1 else 0))
               , ("eqri", riInstruction (\a b -> if a == b then 1 else 0))
               , ("eqrr", rrInstruction (\a b -> if a == b then 1 else 0))
               ]

inputParser :: Parser (IpRegister, [Instruction])
inputParser =
  do
    ipReg <- symbol "#ip" *> integer
    instructions <- M.many instructionParser
    return (ipReg, instructions)

instructionParser :: Parser Instruction
instructionParser =
  do
    _instrName <- T.pack <$> lexeme (M.many M.letterChar)
    [_instrA, _instrB, _instrC] <- M.count 3 integer
    return Instruction{..}

getReg :: Registers -> Int -> Int
getReg regs n
  | n == 0 = regs ^. reg0
  | n == 1 = regs ^. reg1
  | n == 2 = regs ^. reg2
  | n == 3 = regs ^. reg3
  | n == 4 = regs ^. reg4
  | n == 5 = regs ^. reg5
  | otherwise = error "Index out of range"

setReg :: Registers -> Int -> Int -> Registers
setReg regs n value
  | n == 0 = regs & reg0 .~ value
  | n == 1 = regs & reg1 .~ value
  | n == 2 = regs & reg2 .~ value
  | n == 3 = regs & reg3 .~ value
  | n == 4 = regs & reg4 .~ value
  | n == 5 = regs & reg5 .~ value
  | otherwise = error "Index out of range"

rrInstruction :: BinaryOp -> OpFun
rrInstruction op inputA inputB outputC regs =
  setReg regs outputC (a `op` b)
  where
    a = getReg regs inputA
    b = getReg regs inputB

riInstruction :: BinaryOp -> OpFun
riInstruction op inputA inputB outputC regs =
  setReg regs outputC (a `op` inputB)
  where a = getReg regs inputA

irInstruction :: BinaryOp -> OpFun
irInstruction op inputA inputB outputC regs =
  setReg regs outputC (inputA `op` b)
  where b = getReg regs inputB

iiInstruction :: BinaryOp -> OpFun
iiInstruction op inputA inputB outputC regs =
  setReg regs outputC (inputA `op` inputB)

runInstructions :: Int -> IpRegister -> [Instruction] -> Maybe Registers
runInstructions reg0Val ipReg instructions =
  trace (show reg0Val) $
  go (Registers reg0Val 0 0 0 0 0) (0 :: Int)
  where
    go regs count
      | count > 200 = Nothing
      | Just instr <- mInstr
      = let opname = (instr ^. instrName)
            f = operations ^?! ix opname
            a = instr ^. instrA
            b = instr ^. instrB
            c = instr ^. instrC
            regs' = f a b c regs
            regs'' = setReg regs' ipReg $ getReg regs' ipReg + 1
        in go regs'' (count+1)
      |  Nothing <- mInstr = Just regs
      where
        ip = getReg regs ipReg
        mInstr = instructions ^? ix ip
