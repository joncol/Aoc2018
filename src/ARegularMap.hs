{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}

module ARegularMap where

import           Control.Applicative ((<|>))
import           Control.Lens (makeLenses, maximumOf, over, set)
import           Control.Monad (void)
import           Control.Monad.State.Lazy (get, gets, modify, put, StateT)
import           Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import           Data.Maybe (fromJust)
import           Linear.V2 (V2(..), _x, _y)
import qualified Text.Megaparsec as M
import qualified Text.Megaparsec.Char as M

import           Util

type Dir = Char
type Pos = V2 Int
type DistMap = Map Pos Int

data RegexState = RegexState { _rsPos          :: Pos
                             , _rsDistMap      :: DistMap
                             , _rsBranchPoints :: [Pos]
                             } deriving (Eq, Show)

initialState :: RegexState
initialState = RegexState (V2 0 0) (Map.singleton (V2 0 0) 0) []

makeLenses ''RegexState

type StateParser = StateT RegexState Parser

regexParser :: StateParser ()
regexParser =
  do
    void $ M.char '^'
    void $ M.some (dirParser <|>
                   openParensParser <|>
                   closeParensParser <|>
                   altParser)
    void $ M.char '$'

dirParser :: StateParser ()
dirParser =
  do
    dir <- M.oneOf ("NESW" :: String)
    RegexState pos distMap branchPoints <- get
    let dist' = fromJust (Map.lookup pos distMap) + 1
    let pos' = moveDir dir pos
    let distMap' = Map.insertWith min pos' dist' distMap
    put $ RegexState pos' distMap' branchPoints

moveDir :: Dir -> Pos -> Pos
moveDir dir
  | dir == 'N' = over _y (+1)
  | dir == 'E' = over _x (+1)
  | dir == 'S' = over _y (subtract 1)
  | dir == 'W' = over _x (subtract 1)
  | otherwise = error "Invalid direction"

openParensParser :: StateParser ()
openParensParser =
  do
    void $ M.char '('
    pos <- gets _rsPos
    modify $ over rsBranchPoints (pos:)

closeParensParser :: StateParser ()
closeParensParser =
  do
    void $ M.char ')'
    p:_ <- gets _rsBranchPoints
    modify $ over rsBranchPoints tail . set rsPos p

altParser :: StateParser ()
altParser =
  do
    void $ M.char '|'
    p <- gets (head . _rsBranchPoints)
    modify $ set rsPos p

getLongestPath :: DistMap -> Maybe Int
getLongestPath = maximumOf traverse

getRemoteRoomCount :: Int -> DistMap -> Int
getRemoteRoomCount minDistance = Map.size . Map.filter (>=minDistance)
