{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module ReposeRecord where

import           Control.Monad (void)
import           Data.Function (on)
import           Data.List (elemIndex, groupBy, sort, transpose)
import           Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import           Data.Maybe (fromJust)
import qualified Data.Text as T
import           Text.Megaparsec ((<|>))
import qualified Text.Megaparsec as M
import qualified Text.Megaparsec.Char as M
import qualified Text.Megaparsec.Char.Lexer as M

import           Util

newtype GuardId = GuardId Int deriving (Eq, Ord, Show)

data GuardEvent = BeginShift GuardId
                | FallAsleep
                | WakeUp
                deriving (Eq, Ord, Show)

data GuardState = Asleep | Awake deriving (Eq, Ord, Show)

type Date = T.Text;
type Minute = Int;
type Hour = Int;

data GuardRecord =
  GuardRecord { grDate   :: Date
              , grHour   :: Hour
              , grMinute :: Minute
              , grEvent  :: GuardEvent
              } deriving (Eq, Ord, Show)

guardRecordParser :: Parser GuardRecord
guardRecordParser = do
  grDate <- T.pack <$> (M.char '[' *> M.count 10 M.anySingle)
  void M.space1
  [grHour, grMinute] <- M.decimal `M.sepBy` M.char ':'
  void $ M.char ']'
  void M.space1
  grEvent <- BeginShift <$> beginShiftParser <|>
             FallAsleep <$ M.string "falls asleep" <|>
             WakeUp     <$ M.string "wakes up"
  return GuardRecord{..}

beginShiftParser :: Parser GuardId
beginShiftParser = do
  void $ M.string "Guard #"
  guardId <- GuardId <$> M.decimal
  void $ M.string " begins shift"
  return guardId

guardSchedule :: [GuardRecord] -> Map GuardId [GuardRecord]
guardSchedule guardRecords =
  fst $ foldl aux (Map.empty, Nothing) (sort guardRecords)
  where
    aux (schedule, currentGuard) gr =
      case grEvent gr of
        BeginShift gid -> (schedule, Just gid)
        _ -> ( Map.insertWith (flip (++))
                              (fromJust currentGuard)
                              [gr] schedule
             , currentGuard )

totalSleepMinutes :: [GuardRecord] -> Int
totalSleepMinutes guardRecords =
  fst $ foldl aux (0, 0) guardRecords
  where
    aux (totalSleep, lastAwake) gr =
      let m = grMinute gr
      in case grEvent gr of
           FallAsleep -> (totalSleep, m)
           WakeUp     -> (totalSleep + m - lastAwake, 0)
           _          -> (totalSleep, lastAwake)

sleepiestGuard :: Map GuardId [GuardRecord] -> GuardId
sleepiestGuard schedule =
  head . Map.keys $ Map.filter (==maxSleep) sleepTable
  where
    sleepTable = Map.map totalSleepMinutes schedule
    maxSleep = maximum . Map.elems $ sleepTable

mostSleptMinute :: [GuardRecord] -> (Minute, Int)
mostSleptMinute gs = (fromJust $ elemIndex maxVal overlap, maxVal)
  where
    byDate = groupBy ((==) `on` ReposeRecord.grDate) gs
    minByMinMatrix = transpose $ map minuteByMinute byDate
    overlap = map (\m -> length $ filter (==Asleep) (minByMinMatrix !! m))
              [0::Minute .. 59]
    maxVal = maximum overlap

minuteByMinute :: [GuardRecord] -> [GuardState]
minuteByMinute gs = take 60 (sleepState ++ repeat Awake)
  where
    sleepState = fst $ foldl aux ([], 0) gs
      where
        aux (sleep, lastTime) gr =
          let m = grMinute gr
          in case grEvent gr of
            FallAsleep -> (sleep ++ replicate (m - lastTime) Awake, m)
            WakeUp     -> (sleep ++ replicate (m - lastTime) Asleep, m)
            _          -> (sleep, lastTime)
