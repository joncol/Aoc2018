{-# LANGUAGE OverloadedStrings #-}

module Main where

import           Control.Arrow ((&&&))
import           Control.Monad.State.Lazy (runStateT)
import qualified Criterion.Main as Crit
import qualified Data.Either as E
import           Data.Function (on)
import           Data.List (maximumBy)
import qualified Data.Map.Strict as Map
import           Data.Maybe (fromJust, mapMaybe)
import qualified Data.Set as Set
import qualified Data.Text as T
import qualified Text.Megaparsec as M
import           Text.Pretty.Simple (pPrint)

import qualified ARegularMap
import qualified AlchemicalReduction
import qualified BeverageBandits
import qualified ChocolateCharts
import qualified ChronalCalibration
import qualified ChronalCharge
import qualified ChronalClassification
import qualified ChronalConversion
import qualified ChronalCoordinates
import qualified Fabric
import qualified GoWithTheFlow
import qualified InventoryManagementSystem
import qualified MarbleMania
import qualified MemoryManeuver
import qualified MineCartMadness
import qualified ReposeRecord
import qualified ReservoirResearch
import qualified SettlersOfTheNorthPole
import qualified SubterraneanSustainability
import qualified SumOfItsParts
import qualified TheStarsAlign
import           Util (readContents)

newtype ProblemNumber = ProblemNumber Int deriving (Eq, Show)

main :: IO ()
main = Crit.defaultMain
       [ Crit.bgroup "solvers" $
         map (\(pn@(ProblemNumber n), solver) -> Crit.bench (show n) $
               Crit.whnfIO $ runSolver pn solver) solvers
       ]

runSolver :: ProblemNumber -> (T.Text -> IO ()) -> IO ()
runSolver (ProblemNumber n) action = do
  readContents ("data/input" ++ show n ++ ".txt") >>= action
  putStrLn ""

solvers :: [(ProblemNumber, T.Text -> IO ())]
solvers =
  [ ( ProblemNumber 1
    , \input -> do
        let changes = E.rights $
              map (M.parse ChronalCalibration.frequencyChangeParser "") $
                  T.lines input
        print $ ChronalCalibration.applyFrequencyChanges changes
        print $ ChronalCalibration.findFirstRepeatedFrequency changes )
  , ( ProblemNumber 2
    , \input -> do
        let ls = T.lines input
        print $ InventoryManagementSystem.checksum ls
        print $ InventoryManagementSystem.similarBoxes (map T.unpack ls) )
  , ( ProblemNumber 3
    , \input -> do
        let claims = E.rights $ map (M.parse Fabric.claimParser "") $
                                    T.lines input
        print $ Fabric.overlappingSquareInches claims
        print $ Fabric.nonOverlappingClaim claims )
  , ( ProblemNumber 4
    , \input -> do
        let gs = E.rights $ map (M.parse ReposeRecord.guardRecordParser "") $
                                T.lines input
        let schedule = ReposeRecord.guardSchedule gs
        let sleepiest@(ReposeRecord.GuardId sid) =
              ReposeRecord.sleepiestGuard schedule
        let guardSchedule = fromJust $ Map.lookup sleepiest schedule
        let minute = fst $ ReposeRecord.mostSleptMinute guardSchedule
        print $ sid * minute
        let (ReposeRecord.GuardId sid2, (minute2, _)) =
              maximumBy (compare `on` (snd . snd)) .
              Map.toList $ Map.map ReposeRecord.mostSleptMinute schedule
        print $ sid2 * minute2 )
  , ( ProblemNumber 5
    , \input -> do
        let s = T.unpack input
        print . length $ AlchemicalReduction.reducePolymer s
        print $ AlchemicalReduction.removeProblematicUnit s )
  , ( ProblemNumber 6
    , \input -> do
        let cs = E.rights . map (M.parse ChronalCoordinates.coordParser "") $
                                T.lines input
        print $ ChronalCoordinates.largestArea cs
        print $ ChronalCoordinates.safeArea cs 10000 )
  , ( ProblemNumber 7
    , \input -> do
        let stepsDeps =
              E.rights $ map (M.parse SumOfItsParts.stepDependencyParser "") $
                             T.lines input
        print $ SumOfItsParts.executeSteps stepsDeps ['A'..'Z']
        print $ SumOfItsParts.executeParallelSteps stepsDeps 5 60 ['A'..'Z'] )
  , ( ProblemNumber 8
    , \input -> do
        let tree = E.fromRight MemoryManeuver.emptyTree $
                   M.parse MemoryManeuver.treeNodeParser "" input
        print . sum $ MemoryManeuver.allMetadataEntries tree
        print $ MemoryManeuver.getNodeValue tree )
  , ( ProblemNumber 9
    , \input -> do
        let gs = E.fromRight (MarbleMania.GameSetup 0 0) $
                 M.parse MarbleMania.gameSetupParser "" input
        print $ MarbleMania.winningScore gs
        print $
          MarbleMania.winningScore (gs { MarbleMania.gsLastMarble =
                                         100 * MarbleMania.gsLastMarble gs }) )
  , ( ProblemNumber 10
    , \input -> do
        let stars = E.rights $ map (M.parse TheStarsAlign.starParser "") $
                                   T.lines input
        let (alignedStars, idx) = TheStarsAlign.renderAlignedStars stars
        putStrLn . T.unpack $ alignedStars
        print idx )
  , ( ProblemNumber 11
    , \input -> do
        let gridSerialNumber = read $ T.unpack input
        print $ ChronalCharge.getLargestTotalPowerSquarePosition
                gridSerialNumber 300 3
        print . maximum .
                map (\n -> (ChronalCharge.getLargestTotalPowerSquarePosition
                              gridSerialNumber 300) &&& id $ n) $ [1..300] )
  , ( ProblemNumber 12
    , \input -> do
        let plantSetup =
              E.fromRight (SubterraneanSustainability.PlantSetup "" []) $
              M.parse SubterraneanSustainability.plantSetupParser "" $
              input
        let sums =
              map SubterraneanSustainability.sumOfPlantContainingPotIndices $
              SubterraneanSustainability.evolvePlants plantSetup
        print $ sums !! 20
        print $ SubterraneanSustainability.longEvolution 150 50000000000 sums )
  , ( ProblemNumber 13
    , \input -> do
        let mcSetup = E.fromRight [] $
                      M.parse MineCartMadness.mineCartSetupParser "" input
        let carts = MineCartMadness.findCarts $ mcSetup
        let tracks = MineCartMadness.convertTrack $ mcSetup
        print $ MineCartMadness.findFirstCollision tracks carts
        print $ MineCartMadness.runUntilOneCartLeft tracks carts )
  , ( ProblemNumber 14
    , \input -> do
        let n = read $ T.unpack input
        print $ take 10 $ drop n ChocolateCharts.recipes
        let ds = ChocolateCharts.getDigits n
        print $ ChocolateCharts.findSubListIndex ds ChocolateCharts.recipes )
  , ( ProblemNumber 15
    , \input -> do
        let pg = E.fromRight [] $ M.parse BeverageBandits.gridParser "" input
        let units = BeverageBandits.getUnits pg
        let grid = BeverageBandits.convertGrid pg
        -- BeverageBandits.runFightUI 3 grid units
        print $ BeverageBandits.runFight grid units
        print $ BeverageBandits.tuneFight grid units )
  , ( ProblemNumber 16
    , \input -> do
        let (samples, instructions) =
              E.fromRight ([], []) $
              M.parse ChronalClassification.inputParser "" input
        print $ ChronalClassification.getMultiSampleCount samples
        let opcodes = ChronalClassification.findOpcodes samples
        pPrint $ ChronalClassification.runInstructions opcodes instructions )
  , ( ProblemNumber 17
    , \input -> do
        let segments = E.fromRight [] $
                       M.parse ReservoirResearch.segmentsParser "" input
        -- putStrLn . T.unpack $ ReservoirResearch.renderReservoir segments
        let (reached, filled) = ReservoirResearch.floodFill segments
        print $ Set.size reached
        print $ Set.size filled )
  , ( ProblemNumber 18
    , \input -> do
        let area = E.fromRight [] $
                   M.parse SettlersOfTheNorthPole.areaParser "" input

        let gen10 = SettlersOfTheNorthPole.evolveArea area !! 10
        print $ SettlersOfTheNorthPole.getResourceValue gen10

        print $ SettlersOfTheNorthPole.getFutureResourceValue area 1000000000 )
  , ( ProblemNumber 19
    , \input -> do
        let (ipReg, instrs) = E.fromRight (0, []) $
                              M.parse GoWithTheFlow.inputParser "" input

        let regs = GoWithTheFlow.runInstructions ipReg instrs
        print $ GoWithTheFlow.getReg regs 0
        -- See `program19.c` for a solution to the second part
    )
  , ( ProblemNumber 20
    , \input -> do
        let initState = ARegularMap.initialState
        let (_, ARegularMap.RegexState _ distMap _) =
              E.fromRight ((), initState) $
              M.parse (runStateT ARegularMap.regexParser initState) "" input
        print $ ARegularMap.getLongestPath distMap
        print $ ARegularMap.getRemoteRoomCount 1000 distMap )
  , ( ProblemNumber 21
    , \input -> do
        let (ipReg, instrs) = E.fromRight (0, []) $
                              M.parse ChronalConversion.inputParser "" input
        let ns =
              mapMaybe (\n -> ChronalConversion.runInstructions n ipReg instrs)
                       [0..]
        print $ head ns
    )
  ]
