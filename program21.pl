use feature qw(say);

my $reg0 = 0;
my $reg1 = 0;
my $reg2 = 0;
my $reg3 = 0;
my $reg4 = 0;
my $reg5 = 0;

$reg3 = 123;

A:
$reg3 = $reg3 & 456; # 72

if ($reg3 != 72) {
    goto A;
}

B:
$reg5 = $reg3 | 65536;
$reg3 = 15028787;

C:
$reg2 = $reg5 & 255;
$reg3 += $reg2;
$reg3 = $reg3 & 16777215;
$reg3 = $reg3 * 65899;
$reg3 = reg3 & 16777215;

if (256 > $reg5) {
    goto F;
}

$reg2 = 0;
D:
$reg4 = $reg2 + 1;
$reg4 = $reg4 * 256;

if ($reg4 > $reg5) {
    goto E;
}

$reg2 += 1;
goto D;

E:
$reg5 = $reg2;

goto C;

F:
if ($reg3 != $reg0) {
    goto B;
}

say $reg0;
say $reg1;
say $reg2;
say $reg3;
say $reg4;
say $reg5;
