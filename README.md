[![pipeline status](https://gitlab.com/joncol/Aoc2018/badges/master/pipeline.svg)](https://gitlab.com/joncol/Aoc2018/commits/master)

# Aoc2018

## Running Solvers

To run a single solver you can use:

```sh
stack build
stack exec Aoc2018-exe -- -n1 -m pattern 5
```
This will run the solver for problem 5 a single time (no analysis).
