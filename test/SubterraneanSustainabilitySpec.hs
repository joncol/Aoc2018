{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module SubterraneanSustainabilitySpec where

import qualified Data.Either as E
import qualified Data.Text as T
import           Test.Tasty.Hspec
import qualified Text.Megaparsec as M

import           SubterraneanSustainability

inputData :: T.Text
inputData = T.intercalate "\n" ["initial state: #..#.#..##......###...###"
                               , ""
                               , "...## => #"
                               , "..#.. => #"
                               , ".#... => #"
                               , ".#.#. => #"
                               , ".#.## => #"
                               , ".##.. => #"
                               , ".#### => #"
                               , "#.#.# => #"
                               , "#.### => #"
                               , "##.#. => #"
                               , "##.## => #"
                               , "###.. => #"
                               , "###.# => #"
                               , "####. => #"
                               ]

plantSetup :: PlantSetup
plantSetup = head . E.rights . map (M.parse plantSetupParser "") $ [inputData]

spec_subterraneanSustainability :: Spec
spec_subterraneanSustainability = do
  describe "applyRules" $ do
    it "applies a set of rules to evolve plants" $ do
      let PlantSetup{..} = plantSetup
      applyRules psRules (plantStringToMap psInitialState 0)
        `shouldBe` plantStringToMap "#...#....#.....#..#..#..#" 0

  describe "evolvePlants" $ do
    it "applies a set of rules multiple times to evolve plants" $ do
      evolvePlants plantSetup !! 20
        `shouldBe` plantStringToMap "#....##....#####...#######....#.#..##" (-2)

  describe "sumOfPlantContainingPotIndices" $ do
    it "gives the correct result for example" $ do
      let plants = evolvePlants plantSetup !! 20
      sumOfPlantContainingPotIndices plants `shouldBe` 325
