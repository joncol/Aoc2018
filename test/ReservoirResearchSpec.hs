{-# LANGUAGE OverloadedStrings #-}

module ReservoirResearchSpec where

import qualified Data.Either as E
import           Data.Function ((&))
import qualified Data.Set as Set
import qualified Data.Text as T
import           Test.Tasty.Hspec
import qualified Text.Megaparsec as M

import           ReservoirResearch

segmentsData :: T.Text
segmentsData = [ "x=495, y=2..7"
               , "y=7, x=495..501"
               , "x=501, y=3..7"
               , "x=498, y=2..4"
               , "x=506, y=1..2"
               , "x=498, y=10..13"
               , "x=504, y=10..13"
               , "y=13, x=498..504"
               ] & T.intercalate "\n"

spec_reservoirResearch :: Spec
spec_reservoirResearch = do
  describe "horizontalSegmentParser" $ do
    it "successfully parses a horizontal segment" $ do
      M.parse horizontalSegmentParser "" "y=1195, x=558..562"
        `shouldBe` (Right $ HorizontalSegment (Range 558 562) 1195)

  describe "verticalSegmentParser" $ do
    it "successfully parses a vertical segment" $ do
      M.parse verticalSegmentParser "" "x=522, y=879..891"
        `shouldBe` (Right $ VerticalSegment 522 (Range 879 891))

  describe "floodFill" $ do
    it "gives the correct result for example" $ do
      let segments = E.fromRight [] $ M.parse segmentsParser "" segmentsData
      let (reached, filled) = floodFill segments
      Set.size reached `shouldBe` 57
      Set.size filled `shouldBe` 29
