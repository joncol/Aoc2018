{-# LANGUAGE OverloadedStrings #-}

module BeverageBanditsSpec where

import           Control.Arrow ((&&&))
import qualified Data.Either as E
import qualified Data.Text as T
import           Test.Tasty.Hspec
import qualified Text.Megaparsec as M

import           BeverageBandits

parse :: [T.Text] -> (Grid, Units)
parse = (convertGrid &&& getUnits) . pg
  where pg = E.fromRight [] . M.parse gridParser "" . T.intercalate "\n"

sampleGrid1 :: (Grid, Units)
sampleGrid1 = parse [ "#######"
                    , "#.G...#"
                    , "#...EG#"
                    , "#.#.#G#"
                    , "#..G#E#"
                    , "#.....#"
                    , "#######"
                    ]

sampleGrid2 :: (Grid, Units)
sampleGrid2 = parse [ "#######"
                    , "#G..#E#"
                    , "#E#E.E#"
                    , "#G.##.#"
                    , "#...#E#"
                    , "#...E.#"
                    , "#######"
                    ]

sampleGrid3 :: (Grid, Units)
sampleGrid3 = parse [ "#######"
                    , "#E..EG#"
                    , "#.#G.E#"
                    , "#E.##E#"
                    , "#G..#.#"
                    , "#..E#.#"
                    , "#######"
                    ]

sampleGrid4 :: (Grid, Units)
sampleGrid4 = parse [ "#######"
                    , "#E.G#.#"
                    , "#.#G..#"
                    , "#G.#.G#"
                    , "#G..#.#"
                    , "#...E.#"
                    , "#######"
                    ]

sampleGrid5 :: (Grid, Units)
sampleGrid5 = parse [ "#######"
                    , "#.E...#"
                    , "#.#..G#"
                    , "#.###.#"
                    , "#E#G#G#"
                    , "#...#G#"
                    , "#######"
                    ]

sampleGrid6 :: (Grid, Units)
sampleGrid6 = parse [ "#########"
                    , "#G......#"
                    , "#.E.#...#"
                    , "#..##..G#"
                    , "#...##..#"
                    , "#...#...#"
                    , "#.G...G.#"
                    , "#.....G.#"
                    , "#########"
                    ]

-- From https://www.reddit.com/r/adventofcode/comments/a8oprg/2018_day_15_all_examples_work_my_input_doesnt
sampleGrid7 :: (Grid, Units)
sampleGrid7 = parse [ "################################"
                    , "##########.###.###..############"
                    , "##########..##......############"
                    , "#########...##....##############"
                    , "######.....###..G..G############"
                    , "##########..........############"
                    , "##########.............#########"
                    , "#######G..#.G...#......#########"
                    , "#..G##....##..#.G#....#...######"
                    , "##......###..##..####.#..#######"
                    , "#G.G..#..#....#.###...G..#######"
                    , "#.....GG##................######"
                    , "#....G........#####....E.E.#####"
                    , "#####G...#...#######........####"
                    , "####.E#.G...#########.......####"
                    , "#...G.....#.#########......#####"
                    , "#.##........#########.......####"
                    , "######......#########........###"
                    , "######......#########..E.#....##"
                    , "#######..E.G.#######..........##"
                    , "#######E......#####............#"
                    , "#######...G............E.......#"
                    , "####............##............##"
                    , "####..G.........##..........E.##"
                    , "####.G.G#.....####E...##....#.##"
                    , "#######.......####...####..#####"
                    , "########....E....########..#####"
                    , "##########.......#########...###"
                    , "##########.......#########..####"
                    , "##########....############..####"
                    , "###########...##################"
                    , "################################"
                    ]

sampleGrid8 :: (Grid, Units) -- Real input
sampleGrid8 = parse [ "################################"
                    , "##############..###G.G#####..###"
                    , "#######...#####........#.##.####"
                    , "#######..G######.#...........###"
                    , "#######..G..###.............####"
                    , "########.GG.##.G.##.......E#####"
                    , "##########........#........##..#"
                    , "##############GG...#...........#"
                    , "##############.....#..........##"
                    , "#G.G...####....#G......G.#...###"
                    , "#G..#..##........G.........E.###"
                    , "#..###...G#............E.......#"
                    , "#...G...G.....#####............#"
                    , "#....#....#G.#######...........#"
                    , "#.##....#.#.#########.#..#...E.#"
                    , "####...##G..#########.....E...E#"
                    , "#####...#...#########.#.#....E##"
                    , "#####.......#########.###......#"
                    , "######......#########...######.#"
                    , "########.....#######..#..#######"
                    , "########......#####...##.#######"
                    , "########............E.##.#######"
                    , "####.........##......##..#######"
                    , "####....#..E...E...####.########"
                    , "####.....#...........##.########"
                    , "#####....##.#........###########"
                    , "#####.....#####....#############"
                    , "#####.#..######....#############"
                    , "####..######....################"
                    , "####..###.#.....################"
                    , "####...##...####################"
                    , "################################"
                    ]

spec_beverageBandits :: Spec
spec_beverageBandits = do
  describe "runFight" $ do
    it "gives correct result for example 1" $ do
      uncurry runFight sampleGrid1 `shouldBe` 27730
    it "gives correct result for example 2" $ do
      uncurry runFight sampleGrid2 `shouldBe` 36334
    it "gives correct result for example 3" $ do
      uncurry runFight sampleGrid3 `shouldBe` 39514
    it "gives correct result for example 4" $ do
      uncurry runFight sampleGrid4 `shouldBe` 27755
    it "gives correct result for example 5" $ do
      uncurry runFight sampleGrid5 `shouldBe` 28944
    it "gives correct result for example 6" $ do
      uncurry runFight sampleGrid6 `shouldBe` 18740
    it "gives correct result for example 7" $ do
      uncurry runFight sampleGrid7 `shouldBe` 235400
    it "gives correct result for example 8" $ do
      uncurry runFight sampleGrid8 `shouldBe` 222831

  describe "tuneFight" $ do
    it "gives correct result for example" $ do
      uncurry tuneFight sampleGrid8 `shouldBe` 54096
