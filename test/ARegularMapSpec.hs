{-# LANGUAGE OverloadedStrings #-}

module ARegularMapSpec where

import           Control.Monad.State.Lazy (runStateT)
import           Data.Either (fromRight)
import           Test.Tasty.Hspec
import qualified Text.Megaparsec as M

import           ARegularMap

spec_aRegularMap :: Spec
spec_aRegularMap = do
  describe "getLongestPath" $ do
    it "gives the correct result for example 1" $ do
      let (_, RegexState _ distMap _) =
            fromRight ((), initialState) $
            M.parse (runStateT regexParser initialState) "" "^WNE$"
      getLongestPath distMap `shouldBe` Just 3
    it "gives the correct result for example 2" $ do
      let (_, RegexState _ distMap _) =
            fromRight ((), initialState) $
            M.parse (runStateT regexParser initialState) ""
            "^ENWWW(NEEE|SSE(EE|N))$"
      getLongestPath distMap `shouldBe` Just 10
    it "gives the correct result for example 3" $ do
      let (_, RegexState _ distMap _) =
            fromRight ((), initialState) $
            M.parse (runStateT regexParser initialState) ""
            "^ENNWSWW(NEWS|)SSSEEN(WNSE|)EE(SWEN|)NNN$"
      getLongestPath distMap `shouldBe` Just 18

  describe "getRemoteRoomCount" $ do
    it "gives the correct result for example 1" $ do
      let (_, RegexState _ distMap _) =
            fromRight ((), initialState) $
            M.parse (runStateT regexParser initialState) "" "^WNE$"
      getRemoteRoomCount 1 distMap `shouldBe` 3
      getRemoteRoomCount 2 distMap `shouldBe` 2
      getRemoteRoomCount 3 distMap `shouldBe` 1
      getRemoteRoomCount 4 distMap `shouldBe` 0
    it "gives the correct result for example 2" $ do
      let (_, RegexState _ distMap _) =
            fromRight ((), initialState) $
            M.parse (runStateT regexParser initialState) ""
            "^ENWWW(NEEE|SSE(EE|N))$"
      getRemoteRoomCount 1 distMap `shouldBe` 15
      getRemoteRoomCount 2 distMap `shouldBe` 14
      getRemoteRoomCount 3 distMap `shouldBe` 13
      getRemoteRoomCount 4 distMap `shouldBe` 12
      getRemoteRoomCount 5 distMap `shouldBe` 11
      getRemoteRoomCount 6 distMap `shouldBe` 10
      getRemoteRoomCount 7 distMap `shouldBe` 8
      getRemoteRoomCount 8 distMap `shouldBe` 6
      getRemoteRoomCount 9 distMap `shouldBe` 4
      getRemoteRoomCount 10 distMap `shouldBe` 1
    it "gives the correct result for example 3" $ do
      let (_, RegexState _ distMap _) =
            fromRight ((), initialState) $
            M.parse (runStateT regexParser initialState) ""
            "^ENNWSWW(NEWS|)SSSEEN(WNSE|)EE(SWEN|)NNN$"
      getRemoteRoomCount 1 distMap `shouldBe` 24
      getRemoteRoomCount 9 distMap `shouldBe` 15
      getRemoteRoomCount 10 distMap `shouldBe` 13
    it "gives the correct result for example 4" $ do
      let (_, RegexState _ distMap _) =
            fromRight ((), initialState) $
            M.parse (runStateT regexParser initialState) ""
            "^ESSWWN(E|NNENN(EESS(WNSE|)SSS|WWWSSSSE(SW|NNNE)))$"
      getRemoteRoomCount 1 distMap `shouldBe` 35
      getRemoteRoomCount 2 distMap `shouldBe` 34
      getRemoteRoomCount 3 distMap `shouldBe` 33
      getRemoteRoomCount 4 distMap `shouldBe` 32
      getRemoteRoomCount 5 distMap `shouldBe` 31
      getRemoteRoomCount 6 distMap `shouldBe` 30
      getRemoteRoomCount 7 distMap `shouldBe` 29
      getRemoteRoomCount 8 distMap `shouldBe` 27
      getRemoteRoomCount 9 distMap `shouldBe` 26
      getRemoteRoomCount 10 distMap `shouldBe` 25
      getRemoteRoomCount 11 distMap `shouldBe` 24
      getRemoteRoomCount 12 distMap `shouldBe` 23
      getRemoteRoomCount 13 distMap `shouldBe` 21
      getRemoteRoomCount 14 distMap `shouldBe` 19
      getRemoteRoomCount 15 distMap `shouldBe` 17
      getRemoteRoomCount 16 distMap `shouldBe` 15
      getRemoteRoomCount 17 distMap `shouldBe` 12
      getRemoteRoomCount 18 distMap `shouldBe` 9
      getRemoteRoomCount 19 distMap `shouldBe` 7
      getRemoteRoomCount 20 distMap `shouldBe` 6
      getRemoteRoomCount 21 distMap `shouldBe` 4
      getRemoteRoomCount 22 distMap `shouldBe` 2
      getRemoteRoomCount 23 distMap `shouldBe` 1
      getRemoteRoomCount 24 distMap `shouldBe` 0

    it "gives the correct result for example 5" $ do
      let (_, RegexState _ distMap _) =
            fromRight ((), initialState) $
            M.parse (runStateT regexParser initialState) ""
            "^WSSEESWWWNW(S|NENNEEEENN(ESSSSW(NWSW|SSEN)|WSWWN(E|WWS(E|SS))))$"
      getRemoteRoomCount 1 distMap `shouldBe` 48
      getRemoteRoomCount 2 distMap `shouldBe` 47
      getRemoteRoomCount 3 distMap `shouldBe` 46
      getRemoteRoomCount 4 distMap `shouldBe` 45
      getRemoteRoomCount 5 distMap `shouldBe` 44
      getRemoteRoomCount 6 distMap `shouldBe` 43
      getRemoteRoomCount 7 distMap `shouldBe` 42
      getRemoteRoomCount 8 distMap `shouldBe` 41
      getRemoteRoomCount 9 distMap `shouldBe` 40
      getRemoteRoomCount 10 distMap `shouldBe` 39
      getRemoteRoomCount 11 distMap `shouldBe` 38
      getRemoteRoomCount 12 distMap `shouldBe` 37
      getRemoteRoomCount 13 distMap `shouldBe` 35
      getRemoteRoomCount 14 distMap `shouldBe` 34
      getRemoteRoomCount 15 distMap `shouldBe` 33
      getRemoteRoomCount 16 distMap `shouldBe` 32
      getRemoteRoomCount 17 distMap `shouldBe` 31
      getRemoteRoomCount 18 distMap `shouldBe` 30
      getRemoteRoomCount 19 distMap `shouldBe` 29
      getRemoteRoomCount 20 distMap `shouldBe` 28
      getRemoteRoomCount 21 distMap `shouldBe` 27
      getRemoteRoomCount 22 distMap `shouldBe` 26
      getRemoteRoomCount 23 distMap `shouldBe` 24
      getRemoteRoomCount 24 distMap `shouldBe` 22
      getRemoteRoomCount 25 distMap `shouldBe` 20
      getRemoteRoomCount 26 distMap `shouldBe` 18
      getRemoteRoomCount 27 distMap `shouldBe` 16
      getRemoteRoomCount 28 distMap `shouldBe` 13
      getRemoteRoomCount 29 distMap `shouldBe` 10
      getRemoteRoomCount 30 distMap `shouldBe` 7
      getRemoteRoomCount 31 distMap `shouldBe` 3
      getRemoteRoomCount 32 distMap `shouldBe` 0
