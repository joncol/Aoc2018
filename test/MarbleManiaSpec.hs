{-# LANGUAGE OverloadedStrings #-}

module MarbleManiaSpec where

import qualified Data.List.PointedList.Circular as C
import           Data.Maybe (fromJust)
import qualified Data.Text as T
import           Test.Tasty.Hspec
import qualified Text.Megaparsec as M

import           MarbleMania

gameSetup :: T.Text
gameSetup = "466 players; last marble is worth 71436 points"

marblesBefore22 :: MarbleList
marblesBefore22 =
  C.moveN 11 $ fromJust $
  C.fromList [ 0, 16, 8, 17, 4, 18, 9, 19, 2, 20, 10, 21, 5, 11, 1, 12, 6
             , 13, 3, 14, 7, 15 ]

marblesAfter22 :: MarbleList
marblesAfter22 =
  C.moveN 13 $ fromJust $
  C.fromList [ 0, 16, 8, 17, 4, 18, 9, 19, 2, 20, 10, 21, 5, 22, 11, 1, 12, 6
             , 13, 3, 14, 7, 15 ]

marblesBefore23 :: MarbleList
marblesBefore23 =
  C.moveN 13 $ fromJust $
  C.fromList [ 0, 16, 8, 17, 4, 18, 9, 19, 2, 20, 10, 21, 5, 22, 11, 1, 12, 6
             , 13, 3, 14, 7, 15 ]

marblesAfter23 :: MarbleList
marblesAfter23 =
  C.moveN 6 $ fromJust $
  C.fromList [ 0, 16, 8, 17, 4, 18, 19, 2, 20, 10, 21, 5, 22, 11, 1, 12, 6
             , 13, 3, 14, 7, 15 ]

spec_marbleMania :: Spec
spec_marbleMania = do
  describe "gameSetupParser" $ do
    it "parses a game setup" $ do
      M.parse gameSetupParser "" gameSetup
        `shouldBe` Right GameSetup { gsPlayerCount = 466, gsLastMarble = 71436 }

  describe "placeMarble" $ do
    it "returns the updated circle after marble 1" $ do
      placeMarble (fromJust $ C.fromList [0]) 1
        `shouldBe` (C.moveN 1 $ fromJust $ C.fromList [0, 1], 0)
    it "returns the updated circle after marble 2" $ do
      placeMarble (C.moveN 1 $ fromJust $ C.fromList [0, 1]) 2
        `shouldBe` (C.moveN 1 $ fromJust $ C.fromList [0, 2, 1], 0)
    it "returns the updated circle after marble 3" $ do
      placeMarble (C.moveN 1 $ fromJust $ C.fromList [0, 2, 1]) 3
        `shouldBe` (C.moveN 3 $ fromJust $ C.fromList [0, 2, 1, 3], 0)
    it "returns the updated circle after marble 22" $ do
      placeMarble marblesBefore22 22 `shouldBe` (marblesAfter22, 0)
    it "returns the updated circle after marble 23" $ do
      placeMarble marblesBefore23 23 `shouldBe` (marblesAfter23, 32)

  describe "winningScore" $ do
    it "gives the correct result for example 1" $ do
      winningScore (GameSetup 10 1618) `shouldBe` 8317
    it "gives the correct result for example 2" $ do
      winningScore (GameSetup 13 7999) `shouldBe` 146373
    it "gives the correct result for example 3" $ do
      winningScore (GameSetup 17 1104) `shouldBe` 2764
    it "gives the correct result for example 4" $ do
      winningScore (GameSetup 21 6111) `shouldBe` 54718
    it "gives the correct result for example 5" $ do
      winningScore (GameSetup 30 5807) `shouldBe` 37305
