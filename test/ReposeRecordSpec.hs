{-# LANGUAGE OverloadedStrings #-}

module ReposeRecordSpec where

import qualified Data.Either as E
import qualified Data.Map.Strict as Map
import           Test.Tasty.Hspec
import qualified Text.Megaparsec as M

import           ReposeRecord

exData :: [GuardRecord]
exData = E.rights $ map (M.parse guardRecordParser "")
                        [ "[1518-11-01 00:00] Guard #10 begins shift"
                        , "[1518-11-01 00:05] falls asleep"
                        , "[1518-11-01 00:25] wakes up"
                        , "[1518-11-01 00:30] falls asleep"
                        , "[1518-11-01 00:55] wakes up"
                        , "[1518-11-01 23:58] Guard #99 begins shift"
                        , "[1518-11-02 00:40] falls asleep"
                        , "[1518-11-02 00:50] wakes up"
                        , "[1518-11-03 00:05] Guard #10 begins shift"
                        , "[1518-11-03 00:24] falls asleep"
                        , "[1518-11-03 00:29] wakes up"
                        , "[1518-11-04 00:02] Guard #99 begins shift"
                        , "[1518-11-04 00:36] falls asleep"
                        , "[1518-11-04 00:46] wakes up"
                        , "[1518-11-05 00:03] Guard #99 begins shift"
                        , "[1518-11-05 00:45] falls asleep"
                        , "[1518-11-05 00:55] wakes up"
                        ]

spec_reposeRecord :: Spec
spec_reposeRecord = do
  describe "guardRecordParser" $ do
    it "parses a begin shift record" $ do
      M.parse guardRecordParser "" "[1518-11-03 00:05] Guard #10 begins shift"
        `shouldBe` Right (GuardRecord { grDate = "1518-11-03"
                                      , grHour = 0
                                      , grMinute = 5
                                      , grEvent = BeginShift (GuardId 10)
                                      })
    it "parses a falls-asleep record" $ do
      M.parse guardRecordParser "" "[1518-11-04 00:36] falls asleep"
        `shouldBe` Right (GuardRecord { grDate = "1518-11-04"
                                      , grHour = 0
                                      , grMinute = 36
                                      , grEvent = FallAsleep
                                      })
    it "parses a wakes-up record" $ do
      M.parse guardRecordParser "" "[1518-11-05 00:55] wakes up"
        `shouldBe` Right (GuardRecord { grDate = "1518-11-05"
                                      , grHour = 0
                                      , grMinute = 55
                                      , grEvent = WakeUp
                                      })
    it "fails for invalid input" $ do
      M.parse guardRecordParser "" "abc" `shouldSatisfy` E.isLeft

  describe "guardSchedule" $ do
    it "returns all events for a guard" $ do
      let s = guardSchedule exData
      (length <$> Map.lookup (GuardId 10) s) `shouldBe` Just 6
      (length <$> Map.lookup (GuardId 99) s) `shouldBe` Just 6

  describe "totalSleepMinutes" $ do
    it "returns the total sleep minutes for a list of records" $ do
      let s = guardSchedule exData
      (totalSleepMinutes <$> (Map.lookup (GuardId 10) s)) `shouldBe` Just 50
      (totalSleepMinutes <$> (Map.lookup (GuardId 99) s)) `shouldBe` Just 30

  describe "sleepiestGuard" $ do
    it "returns the guard with the most slept minutes" $ do
      sleepiestGuard (guardSchedule exData) `shouldBe` GuardId 10
