{-# LANGUAGE OverloadedStrings #-}

module SettlersOfTheNorthPoleSpec where

import           Data.Either (fromRight)
import qualified Data.Text as T
import           Test.Hspec.Megaparsec
import           Test.Tasty.Hspec
import           Text.Megaparsec as M

import           SettlersOfTheNorthPole

area :: T.Text
area = T.intercalate "\n" $ [ ".#.#...|#."
                            , ".....#|##|"
                            , ".|..|...#."
                            , "..|#.....#"
                            , "#.#|||#|#|"
                            , "...#.||..."
                            , ".|....|..."
                            , "||...#|.#|"
                            , "|.||||..|."
                            , "...#.|..|."
                            ]

spec_settlersOfTheNorthPole :: Spec
spec_settlersOfTheNorthPole = do
  describe "areaParser" $ do
    it "parses an area" $ do
      M.parse areaParser "" `shouldSucceedOn` area

  describe "evolve" $ do
    it "gives the correct result for example" $ do
      let a = fromRight [] $ M.parse areaParser "" area
      let a' = evolveArea a !! 10
      getResourceValue a' `shouldBe` 1147
