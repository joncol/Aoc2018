{-# LANGUAGE OverloadedStrings #-}

module MemoryManeuverSpec where

import qualified Data.Text as T
import           Test.Tasty.Hspec
import qualified Text.Megaparsec as M

import           MemoryManeuver

exampleTree :: T.Text
exampleTree = "2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2"

a :: TreeNode
a = TreeNode { treeNodeHeader = Header { headerChildCount = 2
                                       , headerMetadataEntryCount = 3 }
             , treeNodeChildren = [b, c]
             , treeNodeMetadataEntries = [1, 1, 2]
             }

b :: TreeNode
b = TreeNode { treeNodeHeader = Header { headerChildCount = 0
                                       , headerMetadataEntryCount = 3 }
             , treeNodeChildren = []
             , treeNodeMetadataEntries = [10, 11, 12]
             }

c :: TreeNode
c = TreeNode { treeNodeHeader = Header { headerChildCount = 1
                                       , headerMetadataEntryCount = 1 }
             , treeNodeChildren = [d]
             , treeNodeMetadataEntries = [2]
             }

d :: TreeNode
d = TreeNode { treeNodeHeader = Header { headerChildCount = 0
                                       , headerMetadataEntryCount = 1 }
             , treeNodeChildren = []
             , treeNodeMetadataEntries = [99]
             }

spec_memoryManeuver :: Spec
spec_memoryManeuver = do
  describe "treeNodeParser" $ do
    it "parses a tree" $ do
      M.parse treeNodeParser "" exampleTree
        `shouldBe` Right a

  describe "allMetadataEntries" $ do
    it "return a list of all metadata entries" $ do
      (allMetadataEntries <$> M.parse treeNodeParser "" exampleTree)
        `shouldBe` Right [1, 1, 2, 10, 11, 12, 2, 99]

  describe "getNodeValue" $ do
    it "gives the correct result for the example" $ do
      (getNodeValue <$> M.parse treeNodeParser "" exampleTree)
        `shouldBe` Right 66
