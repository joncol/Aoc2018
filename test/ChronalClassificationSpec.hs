{-# LANGUAGE OverloadedStrings #-}

module ChronalClassificationSpec where

import qualified Data.Text as T
import           Test.Tasty.Hspec
import qualified Text.Megaparsec as M

import           ChronalClassification

sample :: T.Text
sample = T.intercalate "\n" [ "Before: [1, 3, 2, 2]"
                            , "6 2 3 2"
                            , "After:  [1, 3, 1, 2]"
                            ]

spec_chronalClassification :: Spec
spec_chronalClassification = do
  describe "sampleParser" $ do
    it "successfully parses a sample" $ do
      M.parse sampleParser "" sample
        `shouldBe` (Right $ Sample (Registers 1 3 2 2)
                                   (Instruction 6 2 3 2)
                                   (Registers 1 3 1 2))
