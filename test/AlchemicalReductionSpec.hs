{-# LANGUAGE OverloadedStrings #-}

module AlchemicalReductionSpec where

import           Test.Tasty.Hspec

import           AlchemicalReduction

spec_alchemicalReduction :: Spec
spec_alchemicalReduction = do
  describe "reducePolymer" $ do
    it "removes adjacent opposite polarities of the same type" $ do
      reducePolymer "aA" `shouldBe` ""
    it "removes all adjacent opposite polarities of the same type" $ do
      reducePolymer "abBA" `shouldBe` ""
    it "does nothing when there are no opposite adjacent polarities" $ do
      reducePolymer "abAB" `shouldBe` "abAB"
