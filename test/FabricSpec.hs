{-# LANGUAGE OverloadedStrings #-}

module FabricSpec where

import qualified Data.Either as E
import           Test.Tasty.Hspec
import qualified Text.Megaparsec as M

import           Fabric

spec_fabric :: Spec
spec_fabric = do
  describe "claimParser" $ do
    it "parses a claim" $ do
      M.parse claimParser "" "#1 @ 565,109: 14x24"
        `shouldBe` Right (Claim { claimId = ClaimId 1
                                , claimX = 565
                                , claimY = 109
                                , claimWidth = 14
                                , claimHeight = 24
                                })
    it "fails for invalid input" $ do
      M.parse claimParser "" "xyz" `shouldSatisfy` E.isLeft
