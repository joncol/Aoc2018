{-# LANGUAGE OverloadedStrings #-}

module SumOfItsPartsSpec where

import qualified Data.Either as E
import qualified Data.Map.Strict as Map
import           Test.Tasty.Hspec
import qualified Text.Megaparsec as M

import           SumOfItsParts

stepDeps :: [StepDependency]
stepDeps = E.rights $ map (M.parse stepDependencyParser "")
           [ "Step C must be finished before step A can begin."
           , "Step C must be finished before step F can begin."
           , "Step A must be finished before step B can begin."
           , "Step A must be finished before step D can begin."
           , "Step B must be finished before step E can begin."
           , "Step D must be finished before step E can begin."
           , "Step F must be finished before step E can begin."
           ]

spec_sumOfItsParts :: Spec
spec_sumOfItsParts = do
  describe "stepDependencyParser" $ do
    it "parses a step dependency" $ do
      M.parse stepDependencyParser ""
              "Step C must be finished before step A can begin."
        `shouldBe` Right ('C', 'A')

  describe "stepDependencies" $ do
    it "creates a dependency map from step dependencies" $ do
      stepDependencies stepDeps `shouldBe`
        Map.fromList [ ('A', "C")
                     , ('B', "A")
                     , ('D', "A")
                     , ('E', "BDF")
                     , ('F', "C")
                     ]

  describe "executeStep" $ do
    it "removes an executed step from dependency map" $ do
      let deps = stepDependencies stepDeps
      executeStep deps 'C' `shouldBe`
        Map.fromList [ ('A', "")
                     , ('B', "A")
                     , ('D', "A")
                     , ('E', "BDF")
                     , ('F', "")
                     ]

  describe "getNextSteps" $ do
    it "finds the next steps to execute" $ do
      let deps = stepDependencies stepDeps
      getNextSteps deps [] ['A'..'F'] `shouldBe` ['C']

  describe "executeSteps" $ do
    it "gives the correct result for the example" $ do
      executeSteps stepDeps ['A'..'F'] `shouldBe` "CABDFE"

  describe "executeParallelSteps" $ do
    it "gives the correct result for the example" $ do
      executeParallelSteps stepDeps 2 0 ['A'..'F'] `shouldBe` (15, "CABFDE")
