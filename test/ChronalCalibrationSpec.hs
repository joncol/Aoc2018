{-# LANGUAGE OverloadedStrings #-}

module ChronalCalibrationSpec where

import qualified Data.Either as E
import           Test.Tasty.Hspec
import qualified Text.Megaparsec as M

import           ChronalCalibration

spec_chronalCalibration :: Spec
spec_chronalCalibration = do
  describe "frequencyChangeParser" $ do
    it "parses a frequency increase" $ do
      M.parse frequencyChangeParser "" "+1" `shouldBe` Right (Increase 1)
    it "parses a frequency decrease" $ do
      M.parse frequencyChangeParser "" "-5" `shouldBe` Right (Decrease 5)
    it "fails for invalid input" $ do
      M.parse frequencyChangeParser "" "*5" `shouldSatisfy` E.isLeft

  describe "applyFrequencyChange" $ do
    it "applies a frequency increase" $ do
      applyFrequencyChange (Increase 10) (Frequency 5) `shouldBe` Frequency 15
    it "applies a frequency decrease" $ do
      applyFrequencyChange (Decrease 2) (Frequency 5) `shouldBe` Frequency 3

  describe "applyFrequencyChanges" $ do
    it "gives correct result for example 1" $ do
      applyFrequencyChanges [ Increase 1
                            , Decrease 2
                            , Increase 3
                            , Increase 1] `shouldBe` Frequency 3
    it "gives correct result for example 2" $ do
      applyFrequencyChanges [ Increase 1
                            , Increase 1
                            , Increase 1] `shouldBe` Frequency 3
    it "gives correct result for example 3" $ do
      applyFrequencyChanges [ Increase 1
                            , Increase 1
                            , Decrease 2] `shouldBe` Frequency 0
    it "gives correct result for example 4" $ do
      applyFrequencyChanges [ Decrease 1
                            , Decrease 2
                            , Decrease 3] `shouldBe` Frequency (-6)

  describe "findFirstRepetition" $ do
    it "finds the first repeated element in a list" $ do
      findFirstRepetition [0::Int, 1, 2, 3, 1, 5] `shouldBe` Just 1
    it "returns Nothing when no repetition is found" $ do
      findFirstRepetition [0::Int, 1, 2, 3, 5] `shouldBe` Nothing
    it "works for infinite lists" $ do
      findFirstRepetition ([0::Int, 1, 2, 3, 2] ++ [5 ..]) `shouldBe` Just 2

  describe "findFirstRepeatedFrequency" $ do
    it "gives correct result for example 1" $ do
      findFirstRepeatedFrequency [Increase 1, Decrease 1]
                                 `shouldBe` Frequency 0
    it "gives correct result for example 2" $ do
      findFirstRepeatedFrequency [ Increase 3
                                 , Increase 3
                                 , Increase 4
                                 , Decrease 2
                                 , Decrease 4 ]
                                 `shouldBe` Frequency 10
    it "gives correct result for example 3" $ do
      findFirstRepeatedFrequency [ Decrease 6
                                 , Increase 3
                                 , Increase 8
                                 , Increase 5
                                 , Decrease 6 ]
                                 `shouldBe` Frequency 5
