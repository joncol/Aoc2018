{-# LANGUAGE OverloadedStrings #-}

module ChronalCoordinatesSpec where

import           Test.Tasty.Hspec
import qualified Text.Megaparsec as M

import           ChronalCoordinates
import           Util

coords :: Either ParseErrorBundle [Coord]
coords = traverse (M.parse coordParser "")
                        [ "1, 1"
                        , "1, 6"
                        , "8, 3"
                        , "3, 4"
                        , "5, 5"
                        , "8, 9"
                        ]

spec_chronalCoordinates :: Spec
spec_chronalCoordinates = do
  describe "coordParser" $ do
    it "parses a coordinate" $ do
      M.parse coordParser "" "1, 2" `shouldBe` Right (1, 2)

  describe "largestArea" $ do
    it "gives correct result for example" $ do
      largestArea <$> coords `shouldBe` Right 17

  describe "safeArea" $ do
    it "gives correct result for example" $ do
      (flip safeArea 32) <$> coords `shouldBe` Right 16
