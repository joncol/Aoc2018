{-# LANGUAGE OverloadedStrings #-}

module TheStarsAlignSpec where

import qualified Data.Text as T
import           Linear.V2 (V2(..))
import           Test.Tasty.Hspec
import qualified Text.Megaparsec as M

import           TheStarsAlign

star :: T.Text
star = "position=< 7,  0> velocity=<-1,  0>"

spec_theStarsAlign :: Spec
spec_theStarsAlign = do
  describe "starParser" $ do
    it "parses a star" $ do
      M.parse starParser "" star
        `shouldBe` Right Star { starPosition = V2 7 0
                              , starVelocity = V2 (-1) 0 }
