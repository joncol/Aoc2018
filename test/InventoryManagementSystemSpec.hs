{-# LANGUAGE OverloadedStrings #-}

module InventoryManagementSystemSpec where

import qualified Data.Map.Strict as Map
import           Test.Tasty.Hspec

import           InventoryManagementSystem

spec_inventoryManagementSystem :: Spec
spec_inventoryManagementSystem = do
  describe "letterFrequencies" $ do
    it "returns the frequencies of letters in a word" $ do
      letterFrequencies "abra" `shouldBe`
        Map.fromList [('a', 2), ('b', 1), ('r', 1)]

  describe "checksum" $ do
    it "gives correct result for example" $ do
      checksum [ "abcdef"
               , "bababc"
               , "abbcde"
               , "abcccd"
               , "aabcdd"
               , "abcdee"
               , "ababab" ] `shouldBe` 12

  describe "commonElems" $ do
    it "returns the common elements of two strings" $ do
      commonElems "abcdef" "abxdef" `shouldBe` "abdef"

  describe "hammingDistance" $ do
    it "finds the Hamming distance between two strings" $ do
      hammingDistance "abcde" "abcxy" `shouldBe` 2

  describe "similar" $ do
    it "gives correct result for example" $ do
      similarBoxes [ "abcde"
                   , "fghij"
                   , "klmno"
                   , "pqrst"
                   , "fguij"
                   , "axcye"
                   , "wvxyz" ] `shouldBe` "fgij"
