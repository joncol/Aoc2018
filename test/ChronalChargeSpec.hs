{-# LANGUAGE OverloadedStrings #-}

module ChronalChargeSpec where

import           Test.Tasty.Hspec

import           ChronalCharge

spec_chronalCharge :: Spec
spec_chronalCharge = do
  describe "getPowerLevel" $ do
    it "gives the correct result for example 1" $ do
      getPowerLevel 8 (3, 5) `shouldBe` 4
    it "gives the correct result for example 2" $ do
      getPowerLevel 57 (122, 79) `shouldBe` -5
    it "gives the correct result for example 3" $ do
      getPowerLevel 39 (217, 196) `shouldBe` 0
    it "gives the correct result for example 4" $ do
      getPowerLevel 71 (101, 153) `shouldBe` 4

  describe "getStripSumMatrix" $ do
    it "calculates the sum of vertical strips of the input matrix" $ do
      getStripSumMatrix 3
        ([ [1, 2, -1, 4]
         , [-8, -3, 4, 2]
         , [3, 8, 10, -8]
         , [-4, -1, 1, 7]
         ] :: [[Int]]) `shouldBe`
        [ [-4, 7, 13, -2]
        , [-9, 4, 15, 1]
        ]

  describe "getLargestTotalPowerSquarePosition" $ do
    it "gives the correct result for example 1" $ do
      snd (getLargestTotalPowerSquarePosition 18 300 3) `shouldBe` (33, 45)
    it "gives the correct result for example 2" $ do
      snd (getLargestTotalPowerSquarePosition 42 300 3) `shouldBe` (21, 61)
